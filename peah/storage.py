#!/usr/bin/env python3

import os
from django.core.files.storage import FileSystemStorage
from django.conf import settings
import matplotlib.pyplot as mpp

import logging; log = logging.getLogger(__name__)

DFLT_ROOT = os.path.join(settings.MEDIA_ROOT, 'plot')


class PlotStorage(FileSystemStorage):

    def __init__(self, user, DataModel,
                 location=None,
                 base_url=None,
                 file_permissions_mode=None,
                 directory_permissions_mode=None):
        self.user = user
        self.DataModel = DataModel
        root = getattr(settings, 'PLOT_ROOT', DFLT_ROOT)
        dm_name = str(self.DataModel._meta.db_table)
        tail = dm_name
        location = location or os.path.join(root, tail)
        base_url = base_url or '/plot/' + tail
        super(PlotStorage, self).__init__(location=location,
                                          base_url=base_url,
                                          file_permissions_mode=file_permissions_mode,
                                          directory_permissions_mode=directory_permissions_mode)

    def save_plot(self, axis):
        fig = axis.get_figure()
        path = os.path.join(self.location, str(self.user.id) + '.png')
        if not os.path.exists(os.path.dirname(path)):
            os.makedirs(os.path.dirname(path))
        log.debug('Saving {} to {}'.format(axis, path))
        fig.savefig(path)
        mpp.close(fig)

    def url(self):
        return '/media' + self.base_url + str(self.user.id) + '.png'
