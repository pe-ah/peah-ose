###
# Indexes for haystack & elasticsearch
# https://django-haystack.readthedocs.io/en/master/tutorial.html#handling-data
###
import datetime
from haystack import indexes
from peah.models import (
    Company,
    Skill,
    Position,
    Gleaning,
    UploadBasedGleaning,
    RepositoryBasedGleaning,
)


class CompanyIndex(indexes.SearchIndex, indexes.Indexable):
    text = indexes.CharField(document=True, use_template=True)

    def get_model(self):
        return Company

    def index_queryset(self, using=None):
        """Used when the entire index for model is updated."""
        return self.get_model().objects.all()


class SkillIndex(indexes.SearchIndex, indexes.Indexable):
    text = indexes.CharField(document=True, use_template=True)

    def get_model(self):
        return Skill

    def index_queryset(self, using=None):
        return self.get_model().objects.all()


class PositionIndex(indexes.SearchIndex, indexes.Indexable):
    text = indexes.CharField(document=True, use_template=True)

    def get_model(self, using=None):
        return Position

    def index_queryset(self, using=None):
        return self.get_model().objects.all()


def gleaning_index_factory(ModelType):

    class GleaningIndex(indexes.SearchIndex, indexes.Indexable):
        text = indexes.CharField(document=True, use_template=True)
        headline = indexes.CharField(model_attr='headline')
        company = indexes.CharField(model_attr='company__name')
        skills_required = indexes.CharField(model_attr='skills_required__text')
        suitable_for = indexes.CharField(model_attr='suitable_for__text')

        def index_queryset(self, using=None):
            return self.get_model().objects.all()

        def get_model(self):
            return ModelType

    return GleaningIndex


UploadBasedGleaningIndex = gleaning_index_factory(UploadBasedGleaning)
RepositoryBasedGleaningIndex = gleaning_index_factory(RepositoryBasedGleaning)
