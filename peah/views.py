from django.shortcuts import (
    render,
    redirect,
    reverse,
)
from django.views.decorators.http import require_http_methods
from django.contrib.auth.decorators import login_required
from django.core.paginator import Paginator
from django.db.models.expressions import RawSQL
from django.contrib.auth.hashers import check_password
from django.http import (
    HttpResponseForbidden,
)

from haystack.forms import (
    ModelSearchForm,
    SearchForm,
)

from peah.exceptions import (
    PermissionDenied,
    IntegrityError,
)

from django.conf import settings
from django.utils import timezone

from django.contrib.auth.models import (
    Group,
    User,
)
from django.db.models import Count

from peah.models import (
    Company,
    Gleaning,
    RepositoryBasedGleaning,
    UploadBasedGleaning,
    Solution,
    RepositoryBasedSolution,
    UploadBasedSolution,

    Skill,
    Position,

    SolutionComment,
    GleaningComment,

    get_user_companies,
    get_latest_user_solutions,

    StatRequestQueue,
    UserOptions,
    UserData,

    SolutionRating,
)

from peah.forms import (
    CompanyForm,
    RepositoryBasedGleaningForm,
    UploadBasedGleaningForm,
    RepositoryBasedSolutionForm,
    UploadBasedSolutionForm,
    BarePasswordForm,
    UserEditForm,

    GleaningsIndexSearchForm,
    GleaningSearchForm,
)

from haystack.generic_views import (
    SearchView,
)
from haystack.query import SearchQuerySet

from peah.stats import (
    get_user_stats,
)

from peah.util import (
    form_data_to_model_fields,
    update_model,
)

import account.views



import logging; log = logging.getLogger(__name__)


def get_top_skills(limit=10):
    result = Skill.objects\
                  .annotate(frequency=Count('gleanings'))\
                  .order_by('-frequency')\
                  [:limit]
    return result


def get_top_positions(limit=10):
    result = Position.objects\
                     .annotate(frequency=Count('gleanings'))\
                     .order_by('-frequency')\
                     [:limit]
    return result


def get_top_companies(limit=10):
    result = Company.objects\
                    .filter(status=Company.Status.APPROVE)\
                    .annotate(frequency=Count('gleanings'))\
                    .order_by('-frequency')\
                    [:limit]
    return result


class ListGleanings(SearchView):

    PAGINATE_BY=10

    def get_gleaning_queryset(self, qs=SearchQuerySet()):
        q = self.request.GET.get('q', None)
        queryset = qs.models(Gleaning)
        # import pdb; pdb.set_trace()
        if q is not None:
            queryset = queryset.filter(text=q)\
                               .filter_or(headline=q)\
                               .filter_or(company=q)\
                               .filter_or(skills_required=q)\
                               .filter_or(suitable_for=q)
        return queryset

    def get_queryset(self):
        qs = super(ListGleanings, self).get_queryset()
        return self.get_gleaning_queryset()

    def get_form_class(self):
        return GleaningsIndexSearchForm

    def _build_page(self, request=None):
        request = request or self.request
        page_no = request.GET.get('page', 1)
        page_count = request.GET.get('count', 10)
        return self.paginate_queryset(self.get_queryset(), page_count)

    def get_selected_tags(self):

        selected = {
            'position': None,
            'skill': None,
            'company': None,
        }

        (position, skill, company) = [self.request.GET.get(i, None)
                                      for i in ('position', 'skill', 'company')]
        if position and len(position):
            selected['position'] = Position.objects.filter(text=position).first()
        if skill and len(skill):
            selected['skill'] = Skill.objects.filter(text=skill).first()
        if company and len(company):
            selected['company'] = Company.objects.filter(group__name=company).first()
        return selected

    def get_context_data(self, *args, **kwargs):
        context = super(ListGleanings, self).get_context_data(*args, **kwargs)

        top_positions = get_top_positions()
        top_skills = get_top_skills()
        top_companies = get_top_companies()
        # (paginator, page, object_list, is_paginated) = self.build_page()

        # import pdb; pdb.set_trace()
        context.update({
            # 'paginator': paginator,
            # 'page': context['page_obj'] or context['paginator'],
            'top_skills': top_skills,
            'top_positions': top_positions,
            'top_companies': top_companies,
            'selected': self.get_selected_tags(),
            # 'search_form': search_form,
        })
        return context


@login_required
@require_http_methods(['GET', ])
def show_privacy_options_step(request):
    """
    Before the user is completely registed, ask them if they want
    analytics.
    """
    return render(request, 'peah/user/postregister.html')


@login_required
def show_dashboard(request):
    """
    Show the dashboard.

    Show the user dashboard if the user is not a member of a company
    group. Show the company dashboard otherwise.

    :raise: PermissionDenied if user is not authenticated.

    TODO: For now only 1 company is shown, since it's unlikely one
    person will own multiple companies, though this ability is baked
    in to the backend.
    """
    from django.db.models import Q, Count

    if not getattr(request.user, 'options', None):
        return redirect(show_privacy_options_step)

    company = get_user_companies(request.user).first()
    company_solutions = Solution.objects.filter(
        Q(repositorybasedsolution__gleaning__company=company),
        Q(uploadbasedsolution__gleaning__company=company),
        ).order_by('-submitted')

    tpl = 'peah/dashboard/index.html'
    context = {
        'is_staff': request.user.is_staff,
        'company': company,
        'is_superuser': request.user.is_superuser
    }
    if request.user.is_staff:
        queued_companies = Company.objects.filter(status=Company.Status.QUEUED)
        context.update({
            'queued_companies': queued_companies,
            'latest_solutions': Solution.objects.all(),
        })
    if company:
        context.update({
            'gleanings': {
                'open': company.gleanings.annotate(solutions_count=Count('repositorybasedgleaning__solutions')+Count('uploadbasedgleaning__solutions')).order_by('-solutions_count').filter(closed__isnull=True),
                'closed': company.gleanings.filter(closed__isnull=False),
            },
            'solutions': {
                'all': company_solutions,
            }
        })
    else:
        context.update({
            'latest_user_solutions': get_latest_user_solutions(request.user),
            'user_statistics': get_user_stats(request.user),
        })

    stat_requests = StatRequestQueue.objects.filter(user=request.user)
    context.update({
        'stat_requests': stat_requests,
    })

    context['your_solutions'] = Solution.objects\
                                        .filter(user_id=request.user.id)\
                                        .order_by('submitted')
    return render(request, tpl, context)


def show_account(request):
    companies = get_user_companies(request.user).first()
    gleanings = Gleaning.objects.filter(company__in=[companies])
    solutions = Solution.objects.filter(user=request.user)
    return render(request, 'peah/account/index.html', {
        'companies': companies,
        'gleanings': gleanings,
        'solutions': solutions,
    })


@require_http_methods(['GET'])
def show_company(request, id):
    """ Display a company by ID. """
    company = Company.objects.filter(id=id).first()
    if not company:
        return redirect(404)
    return render(request, 'peah/company/index.html', {
        'company': company,
        'can_edit': request.user in company.group.user_set.all(),
    })


@require_http_methods(['POST', 'GET'])
def create_company(request):
    """ Create a company. """
    template = 'peah/company/create.html'
    company = get_user_companies(request.user).first()
    form = CompanyForm()
    if request.method == 'POST':
        form = CompanyForm(request.POST)
        if form.is_valid():
            company = form.save(commit=False)
            group = Group.objects.create(name=request.POST['name'],
                                         user=request.user)
            group.user_set.add(request.user)
            company.group_id = group.id
            company.save()
        return redirect('show_company', company.id)
    return render(request, template, {
        'form': form,
    })


@require_http_methods(['GET', 'POST'])
def edit_company(request, id):
    template = 'peah/company/edit.html'
    companies = get_user_companies(request.user).all()
    company = Company.objects.filter(id=id).first()
    initial_data = {
        'name': company.name,
        'currency': settings.DEFAULT_CURRENCY,
    }
    if company not in companies:
        raise PermissionDenied(3, username=request.user.username)
    form = CompanyForm(initial=initial_data)
    if request.method == 'POST':
        form = CompanyForm(request.POST, instance=company)
        log.debug('form: {}'.format(form))
        log.debug("Current company name: {}".format(company.name))
        log.debug("New company name: {}".format(request.POST['name']))
        if form.is_valid():
            final = form.save()
            final.group.name = request.POST['name']
            final.group.save()
            return redirect('show_company', company.id)
        else:
            log.error('form is not valid: {}'.format(form.errors))
    return render(request, template, {
        'form': form,
        'company': company,
    })


@login_required
@require_http_methods(['GET', 'POST'])
def create_upload_based_gleaning(request):
    """ Create a new gleaning. """
    template = 'peah/gleaning/create/upload.html'
    if request.method == 'POST':
        data = request.POST
        data['company'] = get_user_companies(request.user).first()
        form = UploadBasedGleaningForm(data)


@login_required
@require_http_methods(['GET', 'POST'])
def create_gleaning(request):
    """ Create a new gleaning. """
    template = 'peah/gleaning/create/repository.html'
    data = None

    company = get_user_companies(request.user).first()

    if not company.status == Company.Status.APPROVE:
        raise PermissionDenied(1)

    company = get_user_companies(request.user).first()
    if not company:
        raise PermissionDenied(4, username=request.user.username)

    # Get the type of gleaning
    gleaning_type = request.POST.get('type', request.GET.get('type', None))
    FormClass = UploadBasedGleaningForm
    if gleaning_type == 'repository':
        FormClass = RepositoryBasedGleaningForm
    elif gleaning_type == 'upload':
        FormClass = UploadBasedGleaningForm
    else:
        """ Entry-point to create a gleaning. """
        template = 'peah/gleaning/create/index.html'
        return render(request, template)

    form = FormClass()
    if request.method == 'POST':
        # data = form_data_to_model_fields(RepositoryBasedGleaning, request.POST)
        # data['company_id'] = get_user_companies(request.user).first()
        form = FormClass(request.POST)
        if not form.is_valid():
            log.debug('post data (before): {}'.format(dict(request.POST)))
            log.debug('post data: {}'.format(data))
            log.error(form.errors)
        else:
            gleaning = form.save(commit=False)
            # gleaning.company = company
            gleaning.company_id = company.id
            gleaning.save()
            assert gleaning.company is not None
            return redirect('show_gleaning', gleaning.id)
    return render(request, template, {
        'form': form,
        'type': gleaning_type,
    })


@require_http_methods(['GET', 'POST'])
def close_gleaning(request, id):
    """ Affirm whether to close the gleaning. """
    template = 'peah/gleaning/close.html'
    gleaning = Gleaning.objects.filter(id=id).first()
    if not gleaning.can_edit(request.user):
        raise PermissionDenied(3, username=request.user.username,
                               company_name=gleaning.company.name)
    if request.method == 'GET':
        return render(request, template, {
            'gleaning': gleaning,
        })
    if request.POST.get('yes'):
        if gleaning.closed:
            gleaning.closed = None
        else:
            gleaning.closed = timezone.now()
        gleaning.save()
    return redirect('show_gleaning', gleaning.id)


@require_http_methods(['GET', ])
def show_gleaning(request, id):
    """ Show a gleaning by ID. """
    template = 'peah/gleaning/show.html'
    gleaning = Gleaning.objects.filter(id=id).first()
    # if not gleaning.company.can_view(request.user):
    #     raise PermissionDenied(3,
    #                            username=getattr(request.user, 'username', 'none'),
    #                            company_name=gleaning.company.name)
    solution = None
    if request.user.is_authenticated:
        solution = gleaning.solutions.filter(user=request.user).first()
    return render(request, template, {
        'gleaning': gleaning,
        'solution': solution,
        'can_comment': gleaning.can_comment(request.user),
        'user_in_company': (request.user in gleaning.company.group.user_set.all()),
    })


@require_http_methods(['GET', 'POST'])
def edit_gleaning(request, id):
    template = 'peah/gleaning/edit.html'
    gleaning = Gleaning.objects.filter(id=id).first()
    if any((not gleaning.company.can_view(request.user),
            not gleaning.can_edit(request.user))):
        raise HttpResponseForbidden()

    if isinstance(gleaning, RepositoryBasedGleaning):
        FC = RepositoryBasedGleaningForm
    else:
        FC = UploadBasedGleaningForm

    form = FC(instance=gleaning)
    if request.method == 'POST':
        form = FC(request.POST, instance=gleaning)

    if request.method == 'POST':
        if form.is_valid():
            saved_gleaning = form.save(commit=False)
            saved_gleaning.company_id = gleaning.company.id
            saved_gleaning.save()
            return redirect('show_gleaning', gleaning.id)
        else:
            log.error(form.errors)
    return render(request, template, {
        'gleaning': gleaning,
        'form': form,
    })


@login_required
@require_http_methods(['GET', 'POST'])
def new_solution(request, id):
    gleaning = Gleaning.objects.filter(id=id).first()
    if request.method == 'POST':
        do_save = (request.POST.get('save', None) is not None)
        do_submit = (request.POST.get('submit', None) is not None)
    if isinstance(gleaning, RepositoryBasedGleaning):
        form = RepositoryBasedSolutionForm(request.POST)
    else:
        form = UploadBasedSolutionForm(request.POST)
    log.debug('{}ing new solution'.format(request.method))
    log.debug('POST keys: {}'.format(dict(request.POST).keys()))
    log.debug('GET keys: {}'.format(dict(request.GET).keys()))
    if request.method == 'POST':
        log.debug('do_save = {}, do_submit = {}'.format(do_save, do_submit))
        if form.is_valid():
            if do_submit:
                solution = form.save(commit=False)
                log.debug('SUBMIT solution {}'.format(solution.id))
            elif do_save:
                solution = form.save(commit=False)
                log.debug('SAVE solution {}'.format(solution.id))
            solution.user_id = request.user.id
            solution.gleaning_id = gleaning.id
            solution.save()
            u = '{}?saved={}'.format(reverse('show_solution',
                                             args=[solution.id]),
                                     int(do_save))
            return redirect(u)
        else:
            log.error('form is not valid')
            log.error(form.errors)
    return render(request, 'peah/solution/new.html', {
        'gleaning': gleaning,
        'form': form,
    })


@login_required
@require_http_methods(['GET', 'POST'])
def edit_solution(request, id):
    """ Edit a permission.

    This view will create the form based on the solution type.

    :param request: HTTPRequest

    :param id: ID of the solution

    :return: HttpResponse

    :raise PermissionDenied: if use does not have permission to edit solution
    """
    solution = Solution.objects.filter(id=id).first()
    if not solution.can_edit(request.user):
        raise PermissionDenied(5, username=request.user.username,
                               solution_id=solution.id)
    if solution.submitted is not None:
        # Redirect the user to SHOW the solution.
        log.d('solution already submitted')
        return redirect('show_solution', solution.id)
    # Otherwise, continue constructing the form.
    if isinstance(solution, RepositoryBasedSolution):
        form = RepositoryBasedSolutionForm(instance=solution)
        if request.method == 'POST':
            form = RepositoryBasedSolutionForm(request.POST, instance=solution)
    else:
        form = UploadBasedSolutionForm(instance=solution)
        if request.method == 'POST':
            form = UploadBasedSolutionForm(request.POST, instance=solution)

    if request.method == 'POST':
        if form.is_valid():
            if request.POST.get('submit', False):
                return redirect('confirm_solution', solution.id)
            elif request.POST.get('save', False):
                form.save()
                return redirect('show_solution', solution.id)

    return render(request, 'peah/solution/edit.html', {
        'gleaning': solution.gleaning,
        'form': form,
    })


@login_required
@require_http_methods(['GET', 'POST'])
def withdraw_solution(request, id):
    """ Effectively, confirm the user wants to delete the solution. """
    solution = Solution.objects.filter(id=id).first()
    if not solution.can_edit(request.user):
        raise PermissionDenied(5,
                               username=request.user.username,
                               solution_id=solution.id)
    if solution.submitted is not None:
        # Redirect the user to SHOW the solution.
        log.d('solution already submitted')
        raise PermissionDenied(6)
    if request.method == 'POST':
        url = (reverse('show_solution', args=[solution.id]) +
               '?withdrawn=1')
        if request.POST.get('yes'):
            Solution.objects.delete(id=id)
            u = reverse('show_dashboard') + '?info=gleaning_deleted'
            return redirect(u)
        return redirect(url, solution.id)
    return render(request, 'peah/solution/withdraw.html')


@login_required
@require_http_methods(['GET', 'POST'])
def confirm_solution(request, id):
    solution = Solution.objects.filter(id=id).first()
    if request.user != solution.user:
        raise PermissionDenied(7, username=request.user.username)
    if request.method == 'POST':
        if request.POST.get('yes'):
            solution.submit(request.user)
            solution.save()
            u = reverse('show_solution', args=[solution.id, ])
            u += '?submitted=1'
            return redirect(u)
        else:
            return redirect('edit_solution', solution.id)
    return render(request, 'peah/solution/confirm.html', {
        'solution': solution,
    })


@login_required
@require_http_methods(['GET', ])
def show_solution(request, id):
    solution = Solution.objects.filter(id=id).first()
    if not solution.can_view(request.user):
        raise PermissionDenied(8,
                               username=getattr(request.user, 'username', 0),
                               solution_id=solution.id)
    saved = request.GET.get('saved', 0) == 1
    submitted = request.GET.get('submitted', 0) == 1
    can_select = solution.can_select(request.user)
    template = 'peah/solution/show/user.html'
    if can_select:
        template = 'peah/solution/show/company.html'
    return render(request, template, {
        'solution': solution,
        'upload_based': isinstance(solution, UploadBasedSolution),
        'repository_based': isinstance(solution, RepositoryBasedSolution),
        'submitted': submitted,
        'saved': saved,
        'can_select': can_select,
        'rating': solution.ratings.filter(rater=request.user).first(),
    })


@login_required
@require_http_methods(['GET', ])
def rate_solution(request, solution_id, score):
    solution = Solution.objects.filter(id=solution_id).first()
    next = request.GET.get('next', reverse('show_solution',
                                           args=[solution_id, ]))
    if not solution.can_view(request.user):
        raise PermissionDenied(8,
                               username=getattr(request.user, 'username', 0),
                               solution_id=solution.id)
    rating = SolutionRating.objects.filter(
        solution=solution,
        rater=request.user
    ).first()
    if not rating:
        SolutionRating.objects.create(
            solution_id=solution.id,
            rater=request.user,
            score=int(score)
        )
    else:
        rating.score = int(score)
        rating.save()
    return redirect(next)


@login_required
@require_http_methods(['GET', 'POST'])
def select_solution(request, id):
    """ Select a solution and provide the payout (if it exists).
    """
    solution = Solution.objects.filter(id=id).first()
    if not solution.can_select(request.user):
        log.debug('')
        raise PermissionDenied(9, username=request.user.username,
                               solution_id=solution.id)
    if solution.gleaning.selected:
        raise PermissionDenied(10, gleaning_id=solution.gleaning.id)
    if request.method == 'POST':
        yes = request.POST.get('yes', None) is not None
        # no = request.POST.get('no', None) is not None
        if yes:
            solution.select(request.user)
        return redirect('show_solution', solution.id)
    return render(request, 'peah/solution/select.html', {
        'solution': solution,
    })


@require_http_methods(['GET', ])
def error_403(request, exception):
    tpl = "peah/errors/403/index.html"
    log.debug('kwargs={}'.format(vars(exception)))
    return render(request, tpl, {
        'exception': exception
    })


@login_required
@require_http_methods(['POST', ])
def post_comment(request, comment_type, obj_id):

    if comment_type == 'solution':
        solution = Solution.objects.filter(id=obj_id).first()
        comment = SolutionComment.objects.create(
            user=request.user,
            solution=solution,
            text=request.POST['comment']
        )
    elif comment_type == 'gleaning':
        gleaning = Gleaning.objects.filter(id=obj_id).first()
        comment = GleaningComment.objects.create(
            user=request.user,
            gleaning=gleaning,
            text=request.POST['comment']
        )

    return redirect(request.POST['referer'])


@login_required
@require_http_methods(['GET', ])
def show_user(request, id):

    company = get_user_companies(request.user).first()
    user = User.objects.filter(id=id).first()

    opt = user.options
    req = StatRequestQueue.objects.filter(company=company,
                                          user=user).first()
    denied = False
    stats = get_user_stats(user)
    if ((request.user != user) and
        ((user.options.show_statistics == UserOptions.Show.ON_DEMAND and
         ((not req) or (req.approved is not True))) or
         (user.options.show_statistics == UserOptions.Show.YES))):
        #raise PermissionDenied(12)
        denied = True
        stats = None

    return render(request, 'peah/user/show.html', {
        'in_request': (req is not None),
        'user': user,
        'denied': denied,
        'stats': stats,
    })


@login_required
@require_http_methods(['GET', ])
def request_access(request, user_id):

    company = get_user_companies(request.user).first()
    user = User.objects.filter(id=int(user_id)).first()

    next = request.GET.get('next', None)

    (srq, created) = StatRequestQueue.objects.get_or_create(company=company,
                                                            user=user)
    if not created:
        raise IntegrityError(3)

    if next:
        return redirect(next)
    return redirect('show_user', user_id)


def _do_access_base(request, company_id, approved):

    company = Company.objects.get(id=company_id)
    user = request.user

    next = request.GET.get('next', None)

    srq = StatRequestQueue.objects.filter(company=company,
                                          user=request.user).first()

    if srq.user == request.user:
        srq.approved = approved
        srq.save()

    if next:
        return redirect(next)

    u = reverse('show_dashboard')
    u += '#user-view-requests'
    return redirect(u)


@login_required
@require_http_methods(['GET', ])
def approve_access(request, company_id):
    return _do_access_base(request, company_id, True)


@login_required
@require_http_methods(['GET', ])
def deny_access(request, company_id):
    return _do_access_base(request, company_id, False)


@login_required
@require_http_methods(['POST', ])
def set_user_options(request):
    show_statistics = request.POST.get('show_statistics', None)
    collect_statistics = request.POST.get('collect_statistics', None)

    log.debug('collect_statistics = {}'.format(collect_statistics))

    if show_statistics is not None:
        request.user.options.show_statistics = int(show_statistics)
    request.user.options.collect_statistics = (collect_statistics == 'on')
    log.debug('collect_statistics = {}'.format(request.user.options.collect_statistics))
    request.user.options.save()
    next = request.POST.get('next', reverse('show_dashboard'))
    name = request.POST.get('name', None)
    if name:
        next = next + '#' + name
    return redirect(next)


@login_required
@require_http_methods(['GET', ])
def set_user_analytics(request, do_allow):
    from peah.tasks import ensure_options
    ensure_options(request.user)
    request.user.options.collect_statistics = bool(int(do_allow))
    next = request.GET.get('next', reverse('show_dashboard'))
    name = request.GET.get('name', None)
    if name:
        next = next + '#' + name
    return redirect(next)


@login_required
@require_http_methods(['GET', 'POST'])
def delete_analytics(request):
    form = BarePasswordForm(request.user)
    if request.method == 'POST':
        form = BarePasswordForm(request.user, request.POST)
        if form.is_valid():
            UserData.objects.get(user=request.user).delete()
            return redirect('list_gleanings')
    return render(request, 'peah/user/confirm_delete_analytics.html', {
        'form': form,
        'hello': 'world',
    })


@login_required
@require_http_methods(['GET', 'POST'])
def delete_user_account(request):
    form = BarePasswordForm(request.user)
    if request.method == 'POST':
        form = BarePasswordForm(request.user, request.POST)
        if form.is_valid():
            User.objects.get(id=request.user.id).delete()  # NO TURNING BACK NOW!
            request.session.clear()
            return redirect('list_gleanings')
    return render(request, 'peah/user/confirm_delete_account.html', {
        'form': form,
        'hello': 'world',
    })


@login_required
@require_http_methods(['GET', 'POST'])
def edit_account_info(request):
    initial = {
        'username': request.user.username,
        'email': request.user.email,
    }
    form = UserEditForm(initial=initial, instance=request.user)
    if request.method == 'POST':
        form = UserEditForm(request.POST, instance=request.user)
        if form.is_valid():
            form.save()
        else:
            log.error(form.errors.as_json())
    return render(request, 'peah/user/edit_account.html', {
        'form': form,
        'hello': 'world',
    })
