"""sawmill URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
# from django.contrib import admin
# from peah.admin import AdminableModels as AllModels
# WARNING: DEPRECATION
# Django 2.0 does not use `url`, but django-cms currently requires a Django
# below 2.0. IMPORTANT: MAKE SURE TO FIX THE PATHS WHEN DJANGO-CMS USES DJANGO
# 2.0+
try:
    OLD_DJANGO = True
    from django.urls import (path as path_compat, include)
except ImportError as ie:
    OLD_DJANGO = False
    from django.conf.urls import (url as path_compat, include)
from peah import views as PeahView
# from peah import autocomplete as Autocomplete
from ajax_select import urls as ajax_select_urls


def url_compat(pattern, *args, **kwargs):
    """
    Function to handle deprecation of django's `url` function.
    Because of `path` being deprecated, we'll prepare for it's deprecation.
    Return either the value of `url` or `path`, depending on the django
    version.
    """
    if isinstance(pattern, (tuple, list)):
        if OLD_DJANGO:
            pattern = pattern[1]
        else:
            pattern = pattern[0]
    if OLD_DJANGO:
        return path_compat(pattern, *args, **kwargs)
    return path_compat(pattern, *args, **kwargs)


urlpatterns = [
    url_compat(r'^$', PeahView.ListGleanings.as_view(), name='list_gleanings'),
    url_compat(r'^search$', include('haystack.urls')),
    url_compat(r'^dashboard/$', PeahView.show_dashboard,
               name='show_dashboard'),
    url_compat(r'^postregister/$', PeahView.show_privacy_options_step,
               name='show_privacy_options_step'),
    url_compat(r'^company/new/$', PeahView.create_company, name='new_company'),
    url_compat(r'^company/(?P<id>\d+)/edit$', PeahView.edit_company,
               name='edit_company'),
    url_compat(r'^company/(?P<id>\d+)$', PeahView.show_company,
               name='show_company'),
    url_compat(r'^gleanings/$', PeahView.ListGleanings.as_view(),
               name='manage_gleanings'),
    # url_compat(r'^gleaning/new/repository$',
    #            PeahView.create_repository_based_gleaning,
    #            name='new_repository_gleaning'),
    # url_compat(r'^gleaning/new/upload$',
    #            PeahView.create_upload_based_gleaning,
    #            name='new_upload_gleaning'),
    url_compat(r'^gleaning/new/$', PeahView.create_gleaning,
               name='new_gleaning'),
    url_compat(r'^gleaning/close/(?P<id>\d+)$', PeahView.close_gleaning,
               name='close_gleaning'),
    url_compat(r'^gleaning/edit/(?P<id>\d+)$', PeahView.edit_gleaning,
               name='edit_gleaning'),
    url_compat(r'^gleaning/(?P<id>\d+)$', PeahView.show_gleaning,
               name='show_gleaning'),
    url_compat(r'^solution/new/(?P<id>\d+)$', PeahView.new_solution,
               name='new_solution'),
    url_compat(r'^solution/edit/(?P<id>\d+)$', PeahView.edit_solution,
               name='edit_solution'),
    url_compat(r'^solution/select/(?P<id>\d+)$', PeahView.select_solution,
               name='select_solution'),
    url_compat(r'^solution/submit/(?P<id>\d+)$', PeahView.confirm_solution,
               name='confirm_solution'),
    url_compat(r'^solution/(?P<id>\d+)$', PeahView.show_solution,
               name='show_solution'),
    url_compat(r'^solution/rate/(?P<solution_id>\d+)/(?P<score>\d+)$',
               PeahView.rate_solution,
               name='rate_solution'),

    url_compat(r'^user/(?P<id>\d+)$', PeahView.show_user,
               name='show_user'),

    url_compat(r'^access/request/(?P<user_id>\d+)$', PeahView.request_access,
               name='request_access'),

    url_compat(r'^access/approve/(?P<company_id>\d+)$',
               PeahView.approve_access,
               name='approve_access'),

    url_compat(r'^access/deny/(?P<company_id>\d+)$', PeahView.deny_access,
               name='deny_access'),

    url_compat(r'user/option/set$',
               PeahView.set_user_options,
               name='set_user_options'),

    url_compat(r'user/analytics/set/(?P<do_allow>\d{1})$',
               PeahView.set_user_analytics,
               name='set_user_analytics'),
    url_compat(r'user/delete/account/$',
               PeahView.delete_user_account,
               name='delete_user_account'),
    url_compat(r'user/delete/analytics/$',
               PeahView.delete_analytics,
               name='delete_analytics'),
    url_compat(r'user/update', PeahView.edit_account_info,
               name='edit_account_info'),

    url_compat(r'^comment/(?P<comment_type>\w+)/(?P<obj_id>\d+)$',
               PeahView.post_comment, name='post_comment'),
    # Special autocompletion URLs
    # See https://lstu.fr/sdQn4Er2
    # url_compat(
    #     r'^autocomplete/skill$',
    #     Autocomplete.SkillAutocomplete.as_view(),
    #     name='skill_autocomplete',
    # ),
    url_compat(r'^ajax_select/', include(ajax_select_urls)),
    url_compat(r'^search/', include('haystack.urls')),
    url_compat('avatar/', include('avatar.urls')),
]
