#!/usr/bin/env python3

from django.conf import settings

from django.core.files.storage import (
    default_storage,
)

from django.core.files.base import (
    ContentFile,
)

import os

import uuid

from peah.models import (
    Gleaning,
    Solution,
)

def plot(axis, prefix, name, storage=default_storage, _storage_callback='url'):
    name = str(name)
    if not name.endswith('.png'):
        name = name + '.png'
    outpath = os.path.join('plot', prefix, name)
    full = os.path.join(storage.location, outpath)
    d = os.path.dirname(full)
    if not os.path.exists(d):
        os.makedirs(d)
    fig = axis.get_figure()
    fig.savefig(full)
    return getattr(default_storage, _storage_callback)(outpath)
