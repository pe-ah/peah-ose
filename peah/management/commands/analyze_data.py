import os
import sys

from django.core.management.base import BaseCommand, CommandError
from django.core.management import call_command

from django.contrib.auth.models import (
    User,
    Group,
)

from peah.tasks import (
    analyze_user_statistics
)

import logging; log = logging.getLogger(__name__)


class Command(BaseCommand):

    def add_arguments(self, parser):
        parser.add_argument('-u', '--users', nargs='*',
                            help="List of usernames to analyze")
        parser.add_argument('-g', '--groups', nargs='*',
                            help="Group names of users to analyze")
        parser.add_argument('-f', '--force', action='store_true',
                            default=False,
                            help="Force analytics, only ignoring the timestamp.")

    def handle(self, *args, **kwargs):
        users = User.objects.none()
        has_groups = len(kwargs.get('groups', []) or [])
        has_users = len(kwargs.get('users', []) or [])
        if has_groups:
            users = Group.objects.filter(name__in=kwargs['groups']).user_set
        if has_users:
            users.union(User.objects.filter(username__in=kwargs['user']))
        if not (has_groups or has_users):
            log.info("Generating statistics for all users")

        usernames = [u.username for u in users.values('username').all()]
        log.info('Generating statistics for {} users'.format(len(usernames)))
        analyze_user_statistics(usernames)
