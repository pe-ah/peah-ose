
import os
import sys
import random

from django.core.management.base import BaseCommand, CommandError
from django.core.management import call_command
from django.utils import timezone
from django.apps import apps
from django.contrib import admin
from django.contrib.auth.models import (
    User,
    Group,
)
from django.urls import reverse
from django.contrib.admin.sites import AlreadyRegistered

from django.conf import settings
from peah.fake import (
    fake_user,
    fake_company,
    fake_any_gleaning,
    fake_solution_collection,
    fake_repository_solution,
    fake_position,
    fake_skill,
    fake_user,
    fake_group,
    fake_auto_solution,
    fake,
    fake_peah_environment,
)
from peah import views as PeahViews
from peah.util import (
    random_subset,
)
from peah.models import (
    get_user_companies,
    UserOptions,
)

import logging; log = logging.getLogger(__name__)

RUNDIR = os.path.abspath('.')
DEFAULT_BACKUP_PATH = os.path.join(RUNDIR, 'backups')
MAX = {
    'skill': 50,
    'position': 20,
    'user': 30,
    'company': 10,
}


class Command(BaseCommand):
    """ Fill the connected database with fake Pe'Ah data.

    Used for producing demo.
    """

    help = 'Fill database with demo data.'

    def add_arguments(self, parser):
        parser.add_argument('--backup-location', type=str,
                            help="Location for backup (defaults to current directory, or {})".format(DEFAULT_BACKUP_PATH),
                            default=DEFAULT_BACKUP_PATH)
        parser.add_argument('--show', default=['plain', ],
                            choices=['plain', 'yaml', 'both'],
                            nargs='*',
                            help="Format the output")
        parser.add_argument('--no-backup', default=False, action='store_true')
        parser.add_argument('--force', default=False, action='store_true',
                            help="Force deletion or creation, ignoing any errors.")

    def perform_backup(self, output_path):
        log.info('backing up to {}'.format(output_path))
        call_command('dbbackup', output_path=output_path)

    def remove_all_data(self, force=False):
        total = 0
        all_models = apps.get_app_config('peah').get_models()
        # Since peah depends on users and groups we'll remove all of those
        # as well.
        all_models = list(all_models) + [User, Group]
        app_len = len(all_models)
        tries = int(os.environ.get('PEAH_RETRY', 5))
        while (len(all_models) and tries > 0):
            for ModelClass in all_models:
                c = ModelClass.objects.count()
                n = ModelClass.__name__
                log.info('Deleting {} instances of {}'.format(c, n))
                try:
                    ModelClass.objects.all().delete()
                    all_models.remove(ModelClass)
                except ValueError as e:
                    if not force:
                        log.warn("Try using the '--force', Luke.")
                        raise e
                    if tries == 0:
                        raise e
                    log.warn('Could not delete instances of {}'.format(n))
                    log.warn(e)
                    tries -= 1
                    log.warn('{} tries left'.format(tries))
                total += c
        log.info('deleted {} total instances of {} models'
                 .format(total, app_len))
        failed = len(all_models)
        if failed > 0:
            log.warn('Could not delete {} model types'.format(failed))

    def print_report(self, report):
        from peah.models import Solution
        # Print Out the report

        print('  ! PRINTING THE REPORT  !  ')

        def _print(*args, **kwargs):
            if (len(args) or len(kwargs)):
                sys.stdout.write(*args, **kwargs)
            sys.stdout.write('\n')

        _print("*"*30)
        _print("Fake Data")
        _print("*"*30)
        for k in ['superusers', 'staff', 'desktop']:
            _print("{}".format(k.title()))
            _print("="*len(k))
            for (user, password) in report['users'][k]:
                info = 'company={}, solutions={}, show_stats={}'.format(get_user_companies(user).first(),
                                                                        Solution.objects.filter(user=user).count(),
                                                                        user.options.show_statistics==UserOptions.Show.YES)
                _print('Username: {} [{}]'.format(user.username, info))
                _print('Password: {}'.format(password))
                _print('-'*30)

        _print()
        _print("="*30)
        _print("Approved Companies")
        _print("="*30)
        for company in report['companies']['approved']:
            u = reverse(PeahViews.show_company, args=[company.id])
            _print("{} ({})".format(company.name, u))
        _print()
        _print("="*30)
        _print("Queued Companies")
        _print("="*30)
        for company in report['companies']['approved']:
            if company not in report['companies']['approved']:
                u = reverse(PeahViews.show_company, args=[company.id])
                _print("{} ({})".format(company.name, u))
        _print()
        _print("="*30)
        _print("Closed Gleanings")
        _print("="*30)
        for gleaning in report['gleanings']['closed']:
            u = reverse(PeahViews.show_gleaning, args=[gleaning.id])
            _print("{} ({})".format(gleaning.headline, u))
        _print()
        _print("="*30)
        _print("Gleanings With Solutions")
        _print("="*30)
        for gleaning in report['gleanings']['with_solutions']:
            u = reverse(PeahViews.show_gleaning, args=[gleaning.id])
            _print("{} ({})".format(gleaning.headline, u))
        _print()
        _print("="*30)
        _print("Gleanings With SELECTED Solutions")
        _print("="*30)
        for gleaning in report['gleanings']['with_selected']:
            u = reverse(PeahViews.show_gleaning, args=[gleaning.id])
            _print("{} ({})".format(gleaning.headline, gleaning.id))
        _print()
        _print("="*30)
        _print("Users that submitted solutions")
        _print("="*30)
        for (user, password) in report['users']['submitted_solution']:
            c = Solution.objects.filter(user_id=user.id).count()
            adds = []
            if (user.is_superuser):
                adds.append('is_superuser')
            if (user.is_staff):
                adds.append('is_staff')
            if len(adds):
                _print("{} ({}) [{}]".format(user.username, c, ','.join(adds)))
            else:
                _print("{} ({})".format(user.username, c))
        _print()
        _print("="*30)
        _print("Comments on solutions:")
        _print("="*30)
        for thread in report['comment_threads']['solutions']:
            first_comment = thread[0]
            solution = thread[0].solution
            u = reverse(PeahViews.show_solution, args=[solution.id])
            comment_count = len(thread)
            print("{} comments for solution {} ({})".format(
                comment_count, solution, u
            ))
        _print()
        _print("="*30)
        _print("Comments on gleanings:")
        _print("="*30)
        for thread in report['comment_threads']['gleanings']:
            first_comment = thread[0]
            gleaning = thread[0].gleaning
            u = reverse(PeahViews.show_gleaning, args=[gleaning.id])
            comment_count = len(thread)
            print("{} comments for solution {} ({})".format(
                comment_count, solution, u
            ))

    def generate_fake_data(self, options):
        import yaml

        report = fake_peah_environment()
        show = options.get('show', ['plain'])
        # report = {}

        print('options = {}'.format(options))

        if ('yaml' in show) or ('both' in show):
            print(yaml.dump(report))
        if ('plain' in show) or ('both' in show):
            try:
                self.print_report(report)
            except Exception as e:
                log.error(e)
                raise e

    def handle(self, *args, **options):
        t = timezone.now().strftime('%Y-%m-%d_%H:%M:%S')
        backup_name = '{}_{}.dump'.format(os.path.split(RUNDIR)[-1], t)
        backup_location = options['backup_location']
        if not os.path.exists(backup_location):
            os.makedirs(backup_location)
        backup_path = os.path.join(options['backup_location'], backup_name)
        if not options['no_backup']:
            self.perform_backup(backup_path)
        log.debug('force? {}'.format(options['force']))
        self.remove_all_data(force=options['force'])
        self.generate_fake_data(options)
