from account.hooks import AccountDefaultHookSet
from django.template.loader import render_to_string


import logging; log = logging.getLogger(__name__)


class PeahAccountHookset(AccountDefaultHookSet):

    # def send_invitation_email(to, ctx):
    #     pass
    #
    def send_confirmation_email(self, to, ctx):
        sbj = render_to_string("account/email/email_confirmation_subject.txt", ctx)
        msg = render_to_string("account/email/email_confirmation_message.txt", ctx)
        log.debug('sending confirmation email...')
        log.debug('subj="{}"'.format(sbj))
        log.debug('msg="{}"'.format(msg))
        r = super(PeahAccountHookset, self).send_confirmation_email(to, ctx)
        log.info("Sent confirmation email!")
        log.debug(r)
        return r

    def send_password_change_email(self, to, ctx):
        sbj = render_to_string("account/email/password_change_subject.txt", ctx)
        msg = render_to_string("account/email/password_change.txt", ctx)
        log.debug('sending confirmation email...')
        log.debug('subj="{}"'.format(sbj))
        log.debug('msg="{}"'.format(msg))
        return super(PeahAccountHookset, self).send_password_change_email(to, ctx)
