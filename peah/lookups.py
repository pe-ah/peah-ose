from ajax_select import register, LookupChannel
from peah.models import (
    Skill,
    Position,
)
import re
from django.contrib.postgres.search import TrigramSimilarity

import logging; log = logging.getLogger(__name__)


class TagLookupMixin(object):
    """ Ajax Skill lookup.

    https://django-ajax-selects.readthedocs.io/en/latest/Install.html
    """

    def get_query(self, q, request):
        titles = [x.strip() for x in re.split(r'[\,]', q) if len(x) > 0]
        if (q.endswith(',')):
            new_skill = self.model.objects.get_or_create(text=titles[-1])
            log.debug('add new skill {}'.format(new_skill))
        print('titles q={}'.format(titles))
        qs = []
        # ids = set()
        for t in titles:
            # TODO: construct a better query. The one commented below will
            # create a UNION sql error
            # s = self.model.objects.annotate(
            #         similarity=TrigramSimilarity('text', t)
            #     ).filter(similarity__gt=0.3)
            # qs.append(s)
            s = self.model.objects.annotate(
                    similarity=TrigramSimilarity('text', t)
                ).filter(similarity__gt=0.3)
            if s.count() > 0:
                qs.append(s)
        if len(qs) > 1:
            return qs[0].union(*qs[1:]).order_by('-similarity')
        elif len(qs) == 1:
            return qs[0].order_by('-similarity')
        return self.model.objects.none()

    def format_item_display(self, item):
        return u"<span class='tag'>%s</span>" % item.text

    def format_match(self, obj):
        log.debug(vars(obj))
        return u'<div class="tag-item">{}</div>'.format(obj)


@register('skills')
class SkillLookup(TagLookupMixin, LookupChannel):

    model = Skill


@register('positions')
class PositionLookup(TagLookupMixin, LookupChannel):

    model = Position
