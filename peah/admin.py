from django.contrib import admin

from peah.models import (
    Company,
    UploadBasedGleaning,
    RepositoryBasedGleaning,
    UploadBasedSolution,
    RepositoryBasedSolution,
    Skill,
    Position,
)

AdminableModels = (
    (Company,),
    (Skill,),
    (Position,),
    (UploadBasedGleaning,),
    (RepositoryBasedGleaning,),
    (UploadBasedSolution,),
    (RepositoryBasedSolution,),
)

for AM in AdminableModels:
    admin.site.register(*AM)

# Register your models here.
