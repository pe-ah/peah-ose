from datetime import (
    timedelta
)
from celery import shared_task

import pandas as pd

from django.contrib.auth.models import (
    User,
)

from django.db.models import (
    Avg,
)

from django.conf import settings
from django.utils import timezone

from peah.models import (
    Skill,
    Position,
    Gleaning,
    Solution,
    UserData,
    UserOptions,
    MonthlyCommunicationData,
    WeeklyCommunicationData,
    DailyCommunicationData,
    MonthlySelectedSolutionData,
    DailySelectedSolutionData,

    UserRatingMean,

    SolutionRating,

    SkillCount,
    PositionCount,
)

from peah.stats import (
    get_comment_date_range,
    get_comments_for_month,
    get_comments_for_week,
    get_comments_for_day,
    per_month,
    per_week,
    per_day,
)

import logging; log = logging.getLogger(__name__)


def should_run(user, force=None):
    """ Should we run the data analysis for the user?

    :param user: User we want to check

    :return: True if it's been `settings.SELERY_RUN_INTERVAL` seconds past
        the last run and user has opted in to `collect_statistics`

    """
    interval = getattr(settings, 'CELERY_RUN_INTERVAL', 5 * 60)
    if not getattr(user, 'data', None):
        log.debug('no data')
        return False
    if not user.options.collect_statistics:
        log.debug('opt out of stats')
        return False
    if force:
        log.debug('forcing')
        return True
    if (user.data.last_run + timedelta(seconds=interval)
        > timezone.now()):
        return True
    return True


def ensure_data(user):
    """ Ensure data exists for the user.

    :param user: the user
    """
    if not getattr(user, 'data', None):
        UserData.objects.get_or_create(user=user)


def ensure_options(user):
    if not getattr(user, 'options', None):
        UserOptions.objects.create(user=user)


def _analysis(user_id, DataType, metric_field,
              value_field,
              iter_function,
              get_comment_function,
              force=False):
    user = User.objects.filter(id=user_id).first()
    ensure_data(user)
    ensure_options(user)
    if not should_run(user, force=force):
        return
    (start_date, end_date) = get_comment_date_range(user)
    if None in (start_date, end_date):
        log.warn("No comments for {}".format(user))
        return
    log.debug('WEEKLY: {} - {}'.format(start_date, end_date))
    for i in iter_function(start_date, end_date):
        comments = get_comment_function(user, i)
        data = user.data
        if not comments.count():
            continue
        kwargs = {'data': user.data,
                  value_field: comments.count(),
                  metric_field: i,
                  }
        (mcd, created) = DataType.objects.get_or_create(**kwargs)
        # import pdb; pdb.set_trace()
        log.debug('Data: {}'.format(mcd))
        if created:
            setattr(mcd, metric_field, i)
            setattr(mcd, value_field, comments.count())
            mcd.save()
        return mcd


# For Madagascar fans. ;-)
kowalski = _analysis


@shared_task
def analyze_mean_user_rating(user_id, force=False):
    user = User.objects.filter(id=user_id).first()
    ensure_data(user)
    ensure_options(user)
    if not should_run(user, force=force):
        return
    ratings_qs = SolutionRating.objects.filter(solution__user=user)
    mr = ratings_qs.aggregate(mean=Avg('score'))['mean']
    if not mr:
        log.debug("{} has no ratings".format(user.username))
        return
    urm = UserRatingMean.objects.get_or_create(
        data=user.data,
        mean_rating=(ratings_qs.aggregate(mean=Avg('score'))['mean'] or 0)
    )
    return urm


@shared_task
def analyze_monthly_communication(user_id, force=False):
    return _analysis(user_id, MonthlyCommunicationData,
                     'month',
                     'message_count',
                     per_month,
                     get_comments_for_month,
                     force=force)


@shared_task
def analyze_weekly_communication(user_id, force=False):
    return _analysis(user_id, WeeklyCommunicationData, 'week',
                     'message_count',
                     per_week,
                     get_comments_for_week,
                     force=force)


@shared_task
def analyze_daily_communication(user_id, force=False):
    return _analysis(user_id, DailyCommunicationData, 'day',
                     'message_count',
                     per_day,
                     get_comments_for_day,
                     force=force)


SKILL_COUNT_QUERY = """
SELECT
    peah_skill.text,
    ps.user_id AS p_user,
    solutionskill.skill AS pk,
    COUNT(*) AS skillcount
FROM (
    SELECT p_ubs.solution_ptr_id AS sid1, p_rbs.solution_ptr_id AS sid2, p_gsr.skill_id AS skill
    FROM peah_gleaning_skills_required AS p_gsr
    LEFT OUTER JOIN peah_uploadbasedsolution AS p_ubs
        ON (p_ubs.gleaning_id = p_gsr.gleaning_id)
    LEFT OUTER JOIN peah_repositorybasedsolution AS p_rbs
        ON (p_rbs.gleaning_id = p_gsr.gleaning_id)
)
AS solutionskill
INNER JOIN peah_solution AS ps
    ON (ps.id=solutionskill.sid1 OR ps.id=solutionskill.sid2)
INNER JOIN peah_skill
    ON (solutionskill.skill=peah_skill.text)
WHERE ps.user_id=%s
GROUP BY ps.user_id, solutionskill.skill, peah_skill.text
"""

POSITION_COUNT_QUERY = """
SELECT ps.user_id AS p_user, solutionposition.position AS position, COUNT(*) AS positioncount
FROM (
    SELECT p_ubs.solution_ptr_id AS sid1, p_rbs.solution_ptr_id AS sid2, p_gsr.position_id AS position
    FROM peah_gleaning_suitable_for AS p_gsr
    LEFT OUTER JOIN peah_uploadbasedsolution AS p_ubs
        ON (p_ubs.gleaning_id = p_gsr.gleaning_id)
    LEFT OUTER JOIN peah_repositorybasedsolution AS p_rbs
        ON (p_rbs.gleaning_id = p_gsr.gleaning_id)
)
AS solutionposition
INNER JOIN peah_solution AS ps
ON (ps.id=solutionposition.sid1 OR ps.id=solutionposition.sid2)
WHERE ps.user_id=%s
GROUP BY ps.user_id, solutionposition.position
"""


@shared_task
def analyze_skills_percent(user_id, force=False):
    user = User.objects.filter(id=user_id).first()
    ensure_data(user)
    ensure_options(user)
    if not should_run(user, force=force):
        log.info("not analyzing {}'s statistics".format(user.username))
        return
    # TODO: I'm sure there's a more elegant way to go about this,
    # but for now I'll do it rough and dirty.
    skillset = Skill.objects.raw(SKILL_COUNT_QUERY, [user_id])
    for s in skillset:
        log.debug('skill from skillset: {}'.format(vars(s)))
        (sc, created) = SkillCount.objects.get_or_create(data=user.data,
                                                         skill=Skill.objects.filter(text=s.text).first(),
                                                         value=s.skillcount)
        if created:
            sc.value = s.skillcount
            sc.save()


@shared_task
def analyze_user_statistics(usernames=[], force=False):
    users = User.objects
    if len(usernames):
        users = users.filter(username__in=usernames)
    for user in users.all():
        ensure_data(user)
        ensure_options(user)
        if not should_run(user, force=force):
            log.info("not analyzing {}'s statistics".format(user.username))
            continue
        else:
            log.debug("analyzing {}'s statistics".format(user.username))
        analyze_weekly_communication(user.id, force=force)
        analyze_daily_communication(user.id, force=force)
        analyze_monthly_communication(user.id, force=force)
        analyze_mean_user_rating(user.id, force=force)
        analyze_skills_percent(user.id, force=force)
