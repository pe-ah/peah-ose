from django.db import (
    models
)

from peah.models.mixins import (
    FieldFinderQuerysetMixin,
    PlotOutputFileMixin,
)

from django.db.utils import (
    IntegrityError,
)

import pandas as pd
import logging; log = logging.getLogger(__name__)


class PandasTimeRangeQueryset(FieldFinderQuerysetMixin,
                              PlotOutputFileMixin,
                              models.QuerySet):

    def get_time_field(self):
        return self.find_meta_field_of_type(models.DateField)

    def get_value_field(self):
        return self.find_meta_field_of_type(models.IntegerField)

    def as_daterange_series(self, x=None, y=None):
        data = []
        if not self.count():
            return self.none()
        if not x:
            x = self.get_time_field()
            x = x.name
        if not y:
            y = self.get_value_field()
            y = y.name
        if not all([x, y]):
            raise AttributeError(
                "Could not get x or y. Try specifying them."
            )

        ordered = self.order_by(x)
        data = []
        for i in ordered.values(x, y).all():
            data.append((x, y))
        ts = pd.Series()
        return ts

    def as_daterange_plot(self, x=None, y=None,
                          plot_func_name=None,
                          plot_args=(),
                          plot_kwargs=[]):
        dr = self.as_daterange_plot(x=x, y=y)
        plot_func = dr.plot
        if plot_func_name:
            plot_func = getattr(dr.plot, plot_func_name)
        pl = plot_func(*plot_args, **plot_kwargs)
        fig = pl.get_figure()
        fig.savefig()


class PandasCountedQueryset(FieldFinderQuerysetMixin,
                            PlotOutputFileMixin,
                            models.QuerySet):

    def get_value_field(self):
        return self.find_meta_field_of_type(models.IntegerField)

    def as_series(self):
        data = []
        index = []
        for i in self.all():
            data.append(i.value)
            index.append(i.skill.text)
        log.debug('data={}'.format(data))
        log.debug('index={}'.format(index))
        series = pd.Series(data, index)
        return series


class PandasCountedManager(FieldFinderQuerysetMixin, models.Manager):

    def __init__(self, *args, **kwargs):
        self._model = kwargs.pop('model', None)
        super(PandasCountedManager, self).__init__(*args, **kwargs)

    def get_queryset(self, *args, **kwargs):
        qs = super(PandasCountedManager, self).get_queryset(*args, **kwargs)
        if self._model:
            qs.model = self._model
        return qs

    def get_or_create(self, *args, **kwargs):
        kw = {}
        for f in self.get_fields():
            if (f.name in kwargs and f.unique):
                kw[f.name] = kwargs[f.name]
        i = self.filter(**kw)
        if i.count():
            return (i.get(**kw), False)
        if self.model is None:
            import pdb; pdb.set_trace()
        m = self.model(**kwargs)
        m.save()
        return (m, True)
