import os

from django.db import (
    models
)

from django.core.files.storage import default_storage


import logging; log = logging.getLogger(__name__)

class PlotOutputFileMixin:
    pass


class EasyPrintingMixin:

    def __repr__(self):
        s = "<{} (".format(type(self).__name__)
        fields = []
        for f in type(self)._meta.fields:
            v = getattr(self, f.name, None)
            fields.append('{}={}'.format(f.name, v))
        s = '{}{})>'.format(s, ', '.join(fields))
        return s

    def __str__(self):
        return repr(self)

    def __unicode__(self):
        return repr(self)


class FieldFinderQuerysetMixin:

    def get_fields(self):
        if not self.count():
            return []
        return type(self.first())._meta.fields

    def find_meta_field_of_type(self, *types):
        for f in self.get_fields():
            log.debug("is field {} a {}?".format(f.name, types))
            if isinstance(f, types):
                return f

    def value_from_field(self, field, default=None):
        return getattr(self, field.name, default)
