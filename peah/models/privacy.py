from django.contrib.auth.models import (
    User,
)

from django.db import (
    models,
)


import logging; log = logging.getLogger(__name__)

HT = {
    'ap': 'Is the request approved? None=Not yet, True=Approved, Fale=Denied'
}


class StatRequestQueue(models.Model):

    company = models.ForeignKey('Company', related_name='stat_requests',
                                 primary_key=True)
    user = models.ForeignKey(User, related_name='stat_requests',
                             primary_key=True)
    approved = models.NullBooleanField(blank=True, null=True,
                                       help_text=HT['ap'])
