from django.contrib.auth.models import (
    User,
)

from django.db import (
    models,
)


import logging; log = logging.getLogger(__name__)


class UserOptions(models.Model):

    class Show:
        NO = 0
        YES = 1
        ON_DEMAND = 2
        ALL = (
            (NO, 'No'),
            (YES, 'Yes'),
            (ON_DEMAND, 'Require Approval from Company'),
        )

    user = models.OneToOneField(User, unique=True, related_name='options')
    collect_statistics = models.BooleanField(default=False)
    show_statistics = models.SmallIntegerField(choices=Show.ALL,
                                               default=Show.NO)
