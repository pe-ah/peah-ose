#!/usr/bin/env python3
import os

from django.contrib.auth.models import (
    User,
)

from django.core.files.storage import (
    default_storage,
)

from peah.models.mixins import (
    EasyPrintingMixin
)

from peah.models.managers import (
    PandasTimeRangeQueryset,
    PandasCountedQueryset,
    PandasCountedManager,
)

from django.db import models

from polymorphic.models import PolymorphicModel

"""
TODO: THIS WAS NOT THE INTENTION OF DJANGO!

Data analysis should be done with a big data framework, NOT DJANGO.
This is only for the demo.
"""


class UserData(models.Model):

    last_run = models.DateTimeField(auto_now_add=True)
    user = models.OneToOneField(User, related_name='data', unique=True,
                                primary_key=True)
    def __str__(self):
        return "<{}'s User Data>".format(self.user.username)


class DataBase(EasyPrintingMixin, models.Model, PandasTimeRangeQueryset):

    data = models.ForeignKey(UserData, blank=False, null=False,
                             primary_key=True)
    OUTPATH = None

    objects = PandasTimeRangeQueryset.as_manager()

    def _get_outpath(self, prefix, name, storage=default_storage):
        if not getattr(self, 'OUTPATH', None):
            raise AttributeError('{} needs an `OUTPATH` property'
                                 .format(type(self).__name__))
        name = str(name)
        if not name.endswith('.png'):
            name = name + '.png'
        return os.path.join(self.OUTPATH, prefix, name)

    class Meta:
        abstract = True


class UserRatingMean(DataBase, EasyPrintingMixin):

    mean_rating = models.FloatField(blank=False, null=False)


class CommunicationData(DataBase):
    message_count = models.IntegerField(default=0)

    class Meta:
        abstract = True


class SelectedSolutionData(DataBase):
    selected_solution_count = models.IntegerField(default=0)

    class Meta:
        abstract = True


class MonthlyData(models.Model):
    month = models.DateField()

    class Meta:
        abstract = True


class WeeklyData(models.Model):
    week = models.DateField()

    class Meta:
        abstract = True


class DailyData(models.Model):
    day = models.DateField()

    class Meta:
        abstract = True


class MonthlyCommunicationData(CommunicationData, MonthlyData):
    pass


class WeeklyCommunicationData(CommunicationData, WeeklyData):
    pass


class DailyCommunicationData(CommunicationData, DailyData):
    pass


class MonthlySelectedSolutionData(SelectedSolutionData, MonthlyData):
    pass


class WeeklySelectedSolutionData(SelectedSolutionData, WeeklyData):
    pass


class DailySelectedSolutionData(SelectedSolutionData, DailyData):
    pass


class Countable(DataBase):

    value = models.IntegerField(default=0)
    objects = PandasCountedManager()

    class Meta:
        abstract = True


class SkillCount(Countable):

    skill = models.ForeignKey('Skill', primary_key=True)
    objects = PandasCountedManager.from_queryset(PandasCountedQueryset)()

    class Meta:
        unique_together=(('data', 'skill'),)


class PositionCount(Countable):

    position = models.ForeignKey('Position', primary_key=True)
    objects = PandasCountedManager.from_queryset(PandasCountedQueryset)()

    class Meta:
        unique_together=(('data', 'position'),)
