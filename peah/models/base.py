from datetime import datetime
import logging
from django.contrib.auth.models import (
    AbstractBaseUser as AbstractUser
)
from django.db import models
from django.utils import timezone
from django.db.models.signals import pre_save
from django.dispatch import receiver
from django.contrib.auth.models import (
    User,
    Group,
)
from django.core.validators import (
    MinValueValidator,
    MaxValueValidator
)
from django.core.files.storage import FileSystemStorage
from django.conf import settings
from peah.exceptions import (
    PermissionDenied,
    FieldError,
    IntegrityError,
)

from polymorphic.managers import PolymorphicManager

from mptt.models import MPTTModel
from polymorphic.models import PolymorphicModel
from djmoney.models.fields import (
    MoneyField,
    CurrencyField,
)
from peah.models.mixins import (
    EasyPrintingMixin,
)

from simple_history.models import HistoricalRecords

log = logging.getLogger(__name__)

# Create your models here.

HEADLINE_LENGTH = 1024
COMMENT_LENGTH = 4096
TITLE_LENGTH = 1024
NAME_LENGTH = 1024
INSTRUCTIONS_LENGTH = 4096
DETAILS_LENGTH = 4096
CURRENCY_LENGTH = 5
PAY_DECIMAL_PLACES = 2
PAY_MAX_DIGITS = 10

DEFAULT_RELATED_NAME = '%(app_label)s_%(class)s_items'

# Help texts
HT = {
    # Gleaning.instructions
    'IN': ("What problem are you trying to solve? What are the"
           " requirements of the project?"),
    # Gleaning.suitable_for
    'SF': "Types of positions this for which this gleaning is appropriate.",
    # Gleanings.skills
    'SK': "Skills that are required to fulfill this gleaning.",
    # Gleaning.payout
    'PA': ("The user that submits an acceptable solution is paid this"
           " amount."),
    # Gleaning.payout_hidden
    'PH': ("Should the payout amount be hidden?"),
    # UploadBasedGleaning.maximum_upload_size
    'MU': ("Maximum allowed size for solution (in bytes)"),
}

# Other helpful vars

# Indicates an optional field.
opt = dict(blank=True, null=True)

# Default foreign key arguments.
def_fk = dict(on_delete=models.CASCADE)

# Upload URLs


def gleaning_path(instance, filename):
    return settings.MEDIA_URL + \
        '/gleaning/company_{}/{}'.format(instance.company.id, filename)


def solution_path(instance, filename):
    return settings.MEDIA_URL + \
        '/upload/user_{}/gleaning_{}_{}'.format(instance.user.id,
                                                instance.gleaning.id,
                                                filename)


def get_latest_user_solutions(user):
    return user.solutions.order_by('submitted').all()


def get_user_companies(user):
    if not user:
        return Company.objects.none()
    return Company.objects.filter(group__in=user.groups.all())


class Company(models.Model):
    """ A company or corporation that posts gleanings. """

    class Status:
        QUEUED = 'queued'
        APPROVE = 'approved'
        LOCKED = 'locked'
        _ALL = ((QUEUED, 'Queued'),
                (APPROVE, 'Approved'),
                (LOCKED, 'Locked'))
        CASE_SQL = '''(case when size="{}" then 1
                            when size="{}" then 2
                            when size="{}" then 3
                       end)'''.format('queued', 'approved', 'locked')

    logo = models.ImageField(upload_to='company', **opt)
    group = models.OneToOneField(Group, **def_fk)
    currency = CurrencyField(default=settings.DEFAULT_CURRENCY)

    status = models.CharField(choices=Status._ALL, max_length=10,
                              default=Status.QUEUED)

    history = HistoricalRecords(cascade_delete_history=True)

    def __init__(self, *args, **kwargs):
        self._user = kwargs.pop('user', None)
        self._user_id = kwargs.pop('user_id', None)
        self._name = kwargs.pop('name', None)
        kwargs.pop('group', None)
        kwargs.pop('group_id', None)
        super(Company, self).__init__(*args, **kwargs)

    def save(self, *args, **kwargs):
        user = self._user
        if self._user_id:
            user = User.objects.filter(id=self._user_id).first()
        if not self.pk:
            group_exists = Group.objects.filter(name=self._name).exists()
            if not user:
                raise FieldError(1)
            if group_exists:
                raise IntegrityError(1, name=self._name)
            if not self._name:
                raise IntegrityError(2, fieldname='name')
            self.group = Group.objects.create(name=self._name)
            self.group_id = self.group.id
            # Add user to the group
            # user.is_staff = True
            self.group.user_set.add(user)
            user.save()
        elif self._name:
            self.group.name = self._name
        self.group.save()
        super(Company, self).save(*args, **kwargs)

    @property
    def name(self):
        if getattr(self, 'group', None):
            return self.group.name
        return None

    @name.setter
    def set_name(self, x):
        if self.group:
            self.group.name = x
            self.group.save()
        else:
            self.group = Group.objects.create(name=x)
            self.group_id = self.group.id

    @name.getter
    def get_name(self):
        return self.group.name

    @property
    def is_approved(self):
        return self.status == Company.Status.APPROVE

    def __unicode__(self):
        return str(self)

    def __str__(self):
        return self.name

    class Meta:
        """ Meta class. """

        permissions = (
            ('can_create_company', 'User can create company.'),
            ('can_approve', 'Can approve queued company.'),
        )

    def can_view(self, user):
        return (user and
                (user.is_superuser or
                 self.group in user.groups.all() or
                 self.status == Company.Status.APPROVE)
                 )

    @property
    def open_gleanings(self):
        return self.gleanings.filter(closed=None)


class TagBase(models.Model):

    text = models.CharField(max_length=TITLE_LENGTH, primary_key=True)

    def __unicode__(self):
        return self.text

    def __str__(self):
        return self.text

    def __repr__(self):
        return self.text

    class Meta:
        abstract = True


class Position(TagBase):
    pass


class Skill(TagBase):
    pass


class Gleaning(PolymorphicModel):
    """
    A sample code set; part of a larger project.

    A gleaning in agriculture is the edge of a field, where Jews were
    instructed to practice Pe'Ah.
    """

    company = models.ForeignKey(Company, related_name='gleanings',
                                null=False, blank=False, **def_fk)
    headline = models.CharField(max_length=HEADLINE_LENGTH)
    instructions = models.CharField(max_length=INSTRUCTIONS_LENGTH,
                                    help_text=HT['IN'])
    suitable_for = models.ManyToManyField(Position, help_text=HT['SF'],
                                          blank=True,
                                          related_name='gleanings')
    skills_required = models.ManyToManyField(Skill, help_text=HT['SK'],
                                             blank=True,
                                             related_name='gleanings')
    payout = MoneyField(help_text=HT['PA'],
                        decimal_places=PAY_DECIMAL_PLACES,
                        max_digits=PAY_MAX_DIGITS,
                        **opt)
    payout_hidden = models.BooleanField(default=False,
                                        help_text=HT['PH'])
    if settings.DEBUG:
        posted = models.DateTimeField(default=datetime.now)
    else:
        posted = models.DateTimeField(auto_now=True)
    deadline = models.DateTimeField(**opt)
    closed = models.DateTimeField(auto_now=False, **opt)
    selected_timestamp = models.DateTimeField(**opt)
    selected = None  # implemented in sub-classes

    is_upload_based = False
    is_repository_based = False

    objects = PolymorphicManager()

    # history = HistoricalRecords()

    def can_comment(self, user):
        """ True if the user can comment.

        User can comment if any of the following is true.

        1. User is logged in AND user has began a solution.

        2. User is logged in AND user is member of company with gleaning.

        :param user: request user
        """
        return (user.is_authenticated and
                (
                 any(
                     [s.user == user for s in self.solutions.all()]
                     )
                 ) or (
                    user in self.company.group.user_set.all()
                 ) or (
                    user.is_superuser
                 ) or (
                    user.is_staff
                 ))

    def can_post(self, user):
        return (
            self.company.status == Company.Status.APPROVE
        )

    def can_edit(self, user):
        return (
            self.company.status == Company.Status.APPROVE and
            user in self.company.group.user_set.all()
        )

    def can_view(self, user):
        """ True if user can view this gleaning. """
        return (
            self.company.can_view(user)
        )

    def __unicode__(self):
        return str(self)

    def __str__(self):
        s = ''
        if self.payout and self.payout.amount > 0.0:
            s += '[{}] '.format(self.payout)
        if self.closed:
            s += '[CLOSED] '
        s += '"' + self.headline[:20] + '..." '
        # s += str(self.company or '< no company >')
        return s

    class Meta:
        """ Meta class. """
        permissions = (
            ('can_select', 'User can select a solution'),
        )

    def select(self, user, solution):
        """ Select. """
        if user not in self.company.group.user_set.all():
            print('`{}` not in user set {}'.format(
                user, self.company.group.user_set.all()
            ))
            raise PermissionDenied(3, username=getattr(user, 'username', 'None'),
                                   company_name=self.company.name)
        if not hasattr(self, 'selected'):
            raise PermissionDenied(6)
        self.selected = solution
        self.selected_timestamp = timezone.now()
        self.save()

    @property
    def submitted_solutions(self):
        return self.solutions.filter(submitted__isnull=False)

    def save(self, *args, **kwargs):
        """ Override the model's gleaning. """
        c = Company.objects.filter(id=self.company_id).first()
        if c.status != Company.Status.APPROVE:
            raise PermissionDenied(1)
        super(Gleaning, self).save(*args, **kwargs)


class RepositoryBasedGleaning(Gleaning):
    """ Repository gleaning. """

    repository = models.URLField()
    selected = models.OneToOneField('RepositoryBasedSolution',
                                    blank=True, null=True,
                                    **def_fk)
    is_repository_based = True

    def __unicode__(self):
        return str(self)

    def __str__(self):
        return '🔗 ' + super(RepositoryBasedGleaning, self).__str__()


class UploadBasedGleaning(Gleaning):
    """ Gleaning that contains a file starting point. """

    starting_point = models.FileField(gleaning_path)
    maximum_upload_size = models.IntegerField(help_text=HT['MU'], **opt)
    selected = models.OneToOneField('UploadBasedSolution',
                                    blank=True, null=True, **def_fk)
    is_upload_based = True

    def __unicode__(self):
        return str(self)

    def __str__(self):
        return '📦 ' + super(UploadBasedGleaning, self).__str__()


class Solution(EasyPrintingMixin, PolymorphicModel):
    """ Base class for a solution, or an answer to a gleaning. """

    user = models.ForeignKey(User, related_name='solutions', **def_fk)
    submitted = models.DateTimeField(blank=True, null=True)
    details = models.CharField(max_length=DETAILS_LENGTH, **opt)
    history = HistoricalRecords(cascade_delete_history=True)
    gleaning = None  # Inherited by subclasses

    # objects = PolymorphicManager()

    def can_view(self, user):
        return (
            user and
            user.is_superuser or
            (self.gleaning.company.status == Company.Status.APPROVE and
             (self.user == user or
              user in self.gleaning.company.group.user_set.all())
             ))

    def can_edit(self, user):
        log.debug('user == self.user? {}'.format(user == self.user))
        log.debug('self.submitted is None? {}'.format(self.submitted is None))
        return (self.user == user
                and (self.submitted is None))

    def can_select(self, user):
        return (getattr(self, 'gleaning', None) and
                (self.gleaning.company.status == Company.Status.APPROVE) and
                (self.gleaning.company in get_user_companies(user).all()) and
                (self.submitted is not None))

    def can_comment(self, user):
        return ((getattr(self, 'gleaning', None) and
                 user in self.gleaning.company.group.user_set.all()) or
                user == self.user)


    @property
    def is_selected(self):
        """ True if the solution was chosen as the accepted solution. """
        return (hasattr(self, 'gleaning') and
                self.gleaning and
                self.gleaning.selected == self)

    @property
    def is_rejected(self):
        """ True if solution was NOT chosen, but others among the gleaning
            were chosen.
        """
        return (hasattr(self, 'gleaning') and
                getattr(self.gleaning, 'selected', None) is not None and
                self.gleaning.selected != self)

    @property
    def is_saved(self):
        """ True if the solution is simply saved. """
        return (not self.is_selected and not self.is_rejected)

    def submit(self, user):
        if not self.can_edit(user):
            raise PermissionDenied('{} cannot submit {}'
                                   .format(user, self))
        self.submitted = timezone.now()
        self.save()

    def select(self, user):
        if not self.can_select(user):
            raise PermissionDenied('{} cannot select {}'
                                   .format(user, self))
        self.gleaning.selected = self
        self.gleaning.save()


class SolutionRating(models.Model):

    solution = models.ForeignKey(Solution, related_name='ratings',
                                 blank=False, null=False)
    rater = models.ForeignKey(User, related_name='ratings',
                              blank=False, null=False)
    score = models.SmallIntegerField(validators=[
        MinValueValidator(1),
        MaxValueValidator(5),
    ], blank=False, null=False)

    def __str__(self):
        return "<Rating score={}, user={}, solution_id={}>".format(
            self.score, self.rater.username, self.solution.id
        )


class RepositoryBasedSolution(Solution):
    """ Solution that contains a repository. """

    gleaning = models.ForeignKey(RepositoryBasedGleaning,
                                 related_name='solutions',
                                 on_delete=models.CASCADE)
    repository = models.URLField()


class UploadBasedSolution(Solution):
    """ Solution that contains a file upload. """

    gleaning = models.ForeignKey(UploadBasedGleaning,
                                 related_name='solutions',
                                 on_delete=models.CASCADE)
    file_upload = models.FileField(upload_to=solution_path)


class Comment(PolymorphicModel):

    text = models.TextField(max_length=COMMENT_LENGTH)
    if settings.DEBUG:
        when = models.DateTimeField(default=datetime.now)
    else:
        when = models.DateTimeField(auto_now=True)
    read = models.BooleanField(blank=True, default=False)
    user = models.ForeignKey(User, blank=False, null=False,
                             related_name='comments')

    def __str__(self):
        return '{} {} on {}: "{}..."'.format(
            self.user,
            type(self).__name__,
            self.when,
            self.text[:min(20, len(self.text)-1)]
        )

    def __unicode__(self):
        return str(self)

    class Meta:
        ordering = ['-when']


class GleaningComment(Comment):

    gleaning = models.ForeignKey(Gleaning, blank=False, null=False,
                                 related_name='comments')


class SolutionComment(Comment):

    solution = models.ForeignKey(Solution, blank=False, null=False,
                                 related_name='comments')

    def save(self, *args, **kwargs):
        if not ((self.solution.user == self.user) or
                (self.user in self.solution.gleaning.company
                                                    .group.user_set.all())):
            raise PermissionDenied(11, user=self.user)
        super(SolutionComment, self).save(*args, **kwargs)
