#!/user/bin/env python3

from django.core import exceptions
from django import db

class PeahException(object):

    DETAILS = {}

    def __init__(self, error_code, **format_kwargs):
        self.error_code = error_code
        self.format_kwargs = format_kwargs
        if self.error_code not in self.DETAILS:
            raise AttributeError('error code `{}` not in {}.DETAILS'
                                 .format(self.error_code, type(self).__name__))

    @property
    def details(self):
        return self.DETAILS[self.error_code].format(**self.format_kwargs)

    def __str__(self):
        return repr(self)

    def __unicode__(self):
        return repr(self)

    def __repr__(self):
        return '({}) {}: {}'.format(type(self).__name__,
                                    self.error_code,
                                    self.details)


class PermissionDenied(PeahException, exceptions.PermissionDenied):

    DETAILS = {
        1: "Company is not approved.",
        2: "User is not logged in.",
        3: "User {username} is not a member of the company {company_name}",
        4: "User {username} is not a member of any company",
        5: "User {username} cannot edit solution {solution_id}",
        8: "User {username} cannot view solution {solution_id}",
        9: "User {username} cannot select solution {solution_id}",
        6: "Solution already submitted",
        7: "User {username} is not solution's owner.",
        10: "Gleaning {gleaning_id} already selected a solution.",
        11: "User {user} cannot comment on solution.",
        12: "You do not have permission to see this user's stats.",
    }


class FieldError(PeahException, exceptions.FieldError):

    DETAILS = {
        0: 'Generic',
        1: "Company must be created with a user.",
    }


class IntegrityError(PeahException, db.IntegrityError):

    DETAILS = {
        1: 'Company with the name {name} already exists',
        2: 'Required field: {fieldname}',
        3: 'You have already requested to see the user\'s stats.',
    }
