from cms.app_base import CMSApp
from cms.apphook_pool import apphook_pool
from django.utils.translation import ugettext_lazy as _


@apphook_pool.register
class MyApphook(CMSApp):
    """
    Peah App Hook for Django-CMS.

    http://docs.django-cms.org/en/latest/how_to/apphooks.html
    """

    name = _("Pe'Ah")

    def get_urls(self, page=None, language=None, **kwargs):
        """ Get the URL app hook. """
        return ["peah.urls"]
