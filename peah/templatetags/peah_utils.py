from django import template
from django.template.defaultfilters import stringfilter

register = template.Library()

@register.filter
@stringfilter
def peah_range(val):
    (start, end) = [int(x) for x in val.split(',')]
    return range(start, end)

@register.filter(is_safe=True)
@stringfilter
def reptchar(char, rept):
    return char * int(rept)

@register.filter(is_safe=True)
@stringfilter
def scrub_email(email):
    parts = email.split('@')
    recip = parts[0]
    domain = parts[1].split('.')
    s = recip[0] + ('*' * (len(recip) - 2) + recip[-1]) + '@' + domain[0][0] + ('*' * (len(parts[1])-4)) + '.' + domain[-1]
    return s
