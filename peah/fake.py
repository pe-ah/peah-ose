""" Utility functions to produce fake data.

These functions use python-fake to generate fake data for the database.
"""

import random
import string
import tempfile

from datetime import (
    timedelta,
)
from dateutil import (
    relativedelta,
)
from decimal import Decimal

from faker import Faker

from django.utils import timezone
from django.contrib.auth.models import (
    User,
    Group,
)
from djmoney.money import (
    Money,
)
from django.core.files.base import ContentFile

from peah.models import (
    Company,
    Position,
    Skill,
    RepositoryBasedGleaning as RGleaning,
    UploadBasedGleaning as UGleaning,
    Solution,
    RepositoryBasedSolution as RSolution,
    UploadBasedSolution as USolution,
    GleaningComment,
    SolutionComment,
    UserOptions,
    SolutionRating
)
from peah.util import (
    random_subset,
)

from django.conf import settings

import logging; log = logging.getLogger(__name__)

log = logging.getLogger(__name__)

fake = Faker()


def fake_options(user):
    op = UserOptions.objects.create(
        user=user,
        collect_statistics=fake.boolean(),
        show_statistics=random.choice(range(0, 3))
    )
    return op


def fake_user(password=None, is_staff=False, is_superuser=False,
              include_options=True):
    """ Create a user. """
    username = fake.user_name()
    email = fake.email()
    while User.objects.filter(username=username).exists():
        username = fake.user_name()
    while User.objects.filter(email=email).exists():
        username = fake.user_name()
    u = User.objects.create_user(username,
                                 email,
                                 password or fake.password())
    u.is_staff = is_staff
    u.is_superuser = is_superuser
    if include_options:
        u.options = fake_options(u)
    u.save()
    return u


def fake_group(name=fake.word(), users=[]):
    """ Create a group.

    Users will be genrated automatically.

    :param name: Name of the group. Default will be a random group name.

    """
    if not users:
        users = [
            User.objects.create_user(username=fake.email(),
                                     password=fake.password())
            for i in range(0, 10)
            ]
    group = Group.objects.create(name=name)
    for u in users:
        group.user_set.add(u)
        group.save()
    log.info("Created fake Group: {}".format(group))
    return group


def fake_company(status=None, user=None, additional_users=True):
    company_name = fake.company()
    while Group.objects.filter(name=company_name).exists():
        log.warning('{} exists'.format(company_name))
        company_name = fake.company()
    if not user:
        user = fake_user()
    company = Company.objects.create(
        name=company_name,
        status=(status or Company.Status.QUEUED),
        user=user
    )
    users = []
    if additional_users:
        users = [fake_user() for i in range(0, 10) ]
    for u in users:
        company.group.user_set.add(u)
        company.group.save()
    log.info("Created fake Company: {}".format(company))
    return company


def fake_money(min=0, max=100,
               currencies=None):
    if min < 0:
        min = 0
    if not currencies:
        currencies = tuple(dict(settings.CURRENCY_CHOICES).keys())
    return Money(float(random.randint(min, max)) + random.random(),
                 random.choice(currencies))


def fake_position():
    n = fake.job()
    while Position.objects.filter(text=n).exists():
        log.warn('position {} already exists'.format(n))
        n = fake.job()
    # log.debug('generating position {}'.format(n))
    position = Position.objects.create(text=n)
    log.info("Created fake Position: {}".format(position))
    return position


def fake_skill():
    n = fake.word()
    while Skill.objects.filter(text=n).exists():
        log.warn('skill {} already exists'.format(n))
        n = fake.word()
    # log.debug('generating skill {}'.format(n))
    skill = Skill.objects.create(text=n)
    log.info("Created fake Skill: {}".format(skill))
    return skill


def _fake_gleaning_base(GleaningType,
                        company=None,
                        positions=None,
                        skills=None,
                        preapprove_company=True,
                        deadline_chance=0.5,
                        payout_chance=0.75):
    if not positions:
        positions = [
            fake_position() for i in range(0, 5)
        ]
    if not skills:
        skills = [
            fake_skill() for i in range(0, 5)
        ]
    if not company:
        company = fake_company()

    if preapprove_company:
        company.status = Company.Status.APPROVE
        company.save()

    payout = None
    deadline = None
    if random.random() < payout_chance:
        payout = fake_money()

    # Choose a seemingly-valid `posted` datetime and `deadline`.

    posted = fake.date_time_between(start_date='-100d', end_date='now',
                                    tzinfo=timezone.now().tzinfo)

    log.debug('posted date={}'.format(posted))

    if random.random() < deadline_chance:
        max_date = timezone.now() + relativedelta.relativedelta(years=1)
        deadline = fake.date_time_between_dates(datetime_start=posted,
                                                datetime_end=max_date,
                                                tzinfo=timezone.now().tzinfo)

    kwargs = dict(
        company=company,
        headline=fake.sentence(),
        instructions=fake.paragraph(),
        posted=posted,
        deadline=deadline,
        payout=payout,
        payout_hidden=False,
    )
    if issubclass(GleaningType, RGleaning):
        kwargs['repository'] = fake.url()
    elif issubclass(GleaningType, UGleaning):
        maximum_upload_size = random.randint(1, 25)
        tf = random_temp_file(max(maximum_upload_size, 20)-10)
        starting_point = ContentFile(tf.read())
        kwargs['starting_point'] = starting_point
        kwargs['maximum_upload_size'] = maximum_upload_size
    else:
        raise Exception('Unknown gleaning {}'.format(GleaningType))

    gleaning = GleaningType.objects.create(**kwargs)
    for s in skills:
        gleaning.skills_required.add(s)
        gleaning.save()
    for p in positions:
        gleaning.suitable_for.add(p)
        gleaning.save()
    n = type(GleaningType).__name__
    log.info("Created fake {}: {}".format(n, gleaning))
    return gleaning


def fake_repository_based_gleaning(**kwargs):
    return _fake_gleaning_base(RGleaning, **kwargs)


def fake_upload_based_gleaning(**kwargs):
    return _fake_gleaning_base(UGleaning, **kwargs)


def random_character(sample=string.printable):
    """ Generate a random character.

    :param sample: Sample of characters. Default is `string.printable`

    :return: A single character from `sample`
    """
    return random.choice(sample)


def random_temp_file(max_size=30):
    """ Create a random temp file.

    :param max_size: maximum size of the file, in bytes.

    :return: a file handle to a new temporary file, at most `max_size` bytes.

    """
    sz = random.randint(0, max_size)
    log.info('Generating file of size {}'.format(sz))
    tf = tempfile.NamedTemporaryFile(mode='a+b')
    for i in range(0, sz):
        tf.write(random_character().encode('ascii'))
    tf.seek(0)
    return tf


def fake_decimal(min=0, max=None, decimal_places=2):
    """ Generate a fake python decimal. """
    return Decimal(random.randint(min, max))


def fake_any_gleaning(min_count=1, max_count=10,
                      company_size=5,
                      companies=None,
                      skills=None,
                      positions=None,
                      payout_chance=0.5,
                      deadline_chance=0.75):
    """
    Generate a collection of gleanings, either UploadBased or RepoBased.

    :param min_count: Minimum number of gleanings.

    :param max_count: Maximum number of gleanings.

    :param company_size: Size of the company pool. This is so that we can
    test the N-N relation.

    :return: A collection of fake Gleanings.
    """
    cbs = [fake_upload_based_gleaning,
           fake_repository_based_gleaning, ]
    companies = set(companies or [])
    gleanings = []
    if not companies or len(companies) == 0:
        companies = [
            fake_company() for i in range(0, company_size)
        ]
    c = random.choice(range(min_count, max_count))
    log.info("Creating a collection of {} fake gleanings".format(c))
    chosen_skills = None
    chosen_positions = None
    for i in range(0, c):
        company = random.choice(list(companies))
        chosen_skills = None
        chosen_positions = None
        if skills:
            chosen_skills = random_subset(list(skills))
        if positions:
            chosen_positions = random_subset(list(positions))
        gleanings.append(random.choice(cbs)(company=company,
                                            skills=chosen_skills,
                                            positions=chosen_positions,
                                            payout_chance=payout_chance,
                                            deadline_chance=deadline_chance))
    return gleanings


def fake_repository_solution(company=None,
                             positions=None,
                             skills=None,
                             gleaning=None,
                             user=None,
                             submitted=None,
                             is_submitted=False):
    """ Generate a fake repositoy-based solution.
    If any of the model-esque arguments are eliminated, they will be generated
    automatically.

    :param company: Company that created the gleaning.

    :param positions: Positions used for the gleaning.

    :param skills: Skills used for the gleaning.

    :param gleaning: Gleaning used for the solution. If used, `company`,
        `positions`, and `skills` will be ignored.

    :param user: User submitting the solution.

    :param submitted: When the user has submitted the solution.

    :param is_submitted: True if the solution is already submitted. If True,
        will automatically generated a timestamp in the past, overriding
        `submitted`
    """
    if not gleaning:
        gleaning = fake_repository_based_gleaning(company=company,
                                                  positions=positions,
                                                  skills=skills)

    submitted = None
    if is_submitted:
        submitted = fake.date_time_between_dates(
            datetime_start=gleaning.posted,
            tzinfo=timezone.now().tzinfo
        )
    solution = RSolution.objects.create(
        gleaning=gleaning,
        user=user or fake_user(),
        submitted=submitted,
        details=fake.sentence(),
        repository=fake.url(),
    )
    log.info("Created fake Repository-Based Solution: {}".format(solution))
    return solution


def fake_upload_based_solution(company=None,
                               positions=None,
                               skills=None,
                               gleaning=None,
                               user=None,
                               submitted=None,
                               is_submitted=False):
    if not gleaning:
        gleaning = fake_repository_based_gleaning(company, positions, skills)

    submitted = None
    if is_submitted:
        submitted = fake.date_time_between_dates(
            datetime_start=gleaning.posted,
            tzinfo=timezone.now().tzinfo
        )
    tf = random_temp_file()
    solution_file = ContentFile(tf.read())
    solution = USolution.objects.create(
        gleaning=gleaning,
        user=user or fake_user(),
        submitted=submitted,
        details=fake.sentence(),
        file_upload=solution_file,
    )
    log.info("Created fake Upload-Based Solution: {}".format(solution))
    return solution


def fake_auto_solution(user=None, gleaning=None, submitted=None,
                       is_submitted=False):
    f = fake_upload_based_solution
    if isinstance(gleaning, RGleaning):
        f = fake_repository_solution

    return f(user=user, submitted=submitted, gleaning=gleaning,
             is_submitted=is_submitted)


def fake_comment(user, gleaning=None, solution=None,
                 after_comment=None,
                 interval_min_seconds=60,
                 interval_max_seconds=60*60*24*2):
    text = fake.paragraph()

    if after_comment:
        start_date = after_comment.when
    elif gleaning:
        start_date = gleaning.posted
    else:
        start_date = solution.gleaning.posted

    seconds_range = (interval_min_seconds, interval_max_seconds)
    seconds = random.randint(*seconds_range)
    end_date = start_date + timedelta(seconds=seconds)

    when = fake.date_time_between_dates(
        datetime_start=start_date,
        datetime_end=end_date,
    )

    kwargs = {
        'text': text,
        'when': when,
        'user': user,
    }

    if gleaning:
        CommentType = GleaningComment
        kwargs['gleaning'] = gleaning
    elif solution:
        CommentType = SolutionComment
        kwargs['solution'] = solution
    else:
        raise AttributeError('need either `gleaning` or `solution`')

    comment = CommentType.objects.create(**kwargs)

    log.debug("{}'s comment for {}: '{}'".format(user,
                                                 (gleaning or solution),
                                                 comment))

    return comment


def fake_comment_thread(gleaning=None, solution=None,
                        max_count=50):
    comment_count = random.randint(1, max_count)
    last = None

    user = None
    company_user = None

    if gleaning:
        user = random.choice(list(User.objects.all()))
        company_user = random.choice(list(gleaning.company
                                          .group
                                          .user_set.all()))

    if solution:
        user = solution.user
        company_user = random.choice(list(solution.gleaning.company
                                          .group
                                          .user_set.all()))
    last = None
    comments = []
    for i in range(0, comment_count):
        last = fake_comment(gleaning=gleaning,
                            solution=solution,
                            user=random.choice([user, company_user]),
                            after_comment=last)
        comments.append(last)
    return comments


def fake_solution_collection(max_count_per=3, min_count=1, max_count=20,
                             has_solutions_prob=0.5,
                             solution_submitted_prob=0.5,
                             gleaning_comment_prob=0.5,
                             solution_comment_prob=0.5):
    """ Create a collection of fake solutions.

    :parm max_count_per: Maximum number of solutions per gleaning.

    :param min_count: Minimnum number of solutions per gleaning.

    :max count: Maximum number of soltuions total.
    """

    gleanings = fake_any_gleaning()
    solutions = []
    for i in range(1, random.choice(range(min_count, max_count))):
        gleaning = random.choice(gleanings)
        if isinstance(gleaning, RGleaning):
            gen = fake_repository_solution
        elif isinstance(gleaning, UGleaning):
            gen = fake_upload_based_solution
        else:
            log.warn('unknown gleaning type {}'.format(type(gleaning).__name__))
            continue
        if random.random() < has_solutions_prob:
            for i in range(1, max_count_per):
                is_submitted = random.random() < solution_submitted_prob
                subm = gen(gleaning=gleaning,
                           is_submitted=is_submitted)
                log.debug('generate solution {} for {}'.format(subm, gleaning))
                gleaning.solutions.add(subm)
                gleaning.save()


def fake_peah_environment(min_desktop_users=25,
                          max_desktop_users=50,
                          min_staff=4,
                          max_staff=7,
                          min_superusers=3,
                          max_superusers=4,
                          min_skills=10,
                          max_skills=50,
                          min_positions=10,
                          max_positions=50,
                          chance_submission_submitted=0.5,
                          payout_chance=0.75,
                          deadline_chance=0.4,
                          gleaning_solution_chance_min=0.5,
                          gleaning_solution_chance_max=0.9):
    from peah.models import (
        get_user_companies,
        Company,
    )
    report = {
        'users': {
            'all': [],
            'desktop': [],
            'staff': [],
            'superusers': [],
            'submitted_solution': [],
        },
        'companies': {
            'all': [],
            'approved': [],
        },
        'gleanings': {
            'all': [],
            'with_solutions': [],
            'closed': [],
            'with_selected': [],
        },
        'comment_threads': {
            'all': [],
            'solutions': [],
            'gleanings': [],
        }
    }
    for i in range(0, random.randint(min_desktop_users, max_desktop_users)):
        password = fake.password()
        u = fake_user(password)
        report['users']['desktop'].append((u, password))
        report['users']['all'].append((u, password))
    for i in range(0, random.randint(min_staff, max_staff)):
        password = fake.password()
        u = fake_user(password, is_staff=True)
        report['users']['staff'].append((u, password))
        report['users']['all'].append((u, password))
    for i in range(0, random.randint(min_superusers, max_superusers)):
        password = fake.password()
        u = fake_user(password, is_superuser=True, is_staff=True)
        report['users']['superusers'].append((u, password))
        report['users']['all'].append((u, password))

    skills = []
    positions = []
    for i in range(0, random.randint(min_skills, max_skills)):
        skills.append(fake_skill())
    for i in range(0, random.randint(min_positions, max_positions)):
        positions.append(fake_position())

    # Create some companies
    for (u, password) in random_subset(list(report['users']['all']), k_pct_min=0.3, k_pct_max=0.6):
        c = fake_company(user=u)
        log.debug('{} created {}'.format(u, c))
        report['companies']['all'].append(c)

    # Some of the companies are approved:
    for c in random_subset(list(report['companies']['all']), k_pct_min=0.5):
        c.status = Company.Status.APPROVE
        c.save()
        log.debug('{} is approved'.format(c))
        report['companies']['approved'].append(c)

    # But do it for all the superusers
    for (u, password) in report['users']['superusers']:
        c = get_user_companies(u).first()
        if not c:
            continue
        c.status = Company.Status.APPROVE
        log.debug('{} is approved'.format(c))
        c.save()
        if c not in report['companies']['approved']:
            report['companies']['approved'].append(c)

    # Create some gleanings from the APPROVED companies
    log.debug('Generating gleanings')
    kwargs = dict(
        companies=report['companies']['approved'],
        max_count=100,
        skills=skills, positions=positions,
        payout_chance=payout_chance,
        deadline_chance=deadline_chance
    )
    report['gleanings']['all'] = fake_any_gleaning(**kwargs)
    report['gleanings']['with_solutions'] = random_subset(report['gleanings']['all'], k_min=1)
    report['users']['submitted_solution'] = random_subset(report['users']['desktop'], k_min=1)
    log.debug('finished generating gleanings')

    # Add solutions to the gleanings
    for (u, password) in report['users']['submitted_solution']:
        for g in random_subset(report['gleanings']['with_solutions'],
                               k_pct_min=gleaning_solution_chance_min,
                               k_pct_max=gleaning_solution_chance_max):
            is_submitted = fake.boolean(int(100 * chance_submission_submitted))
            solution = fake_auto_solution(user=u, gleaning=g,
                                          is_submitted=is_submitted)
            log.debug(('{} submitted solution for {} (submitted={})' +
                       '. {} now has {} submissions'
                       ).format(u, g, is_submitted, u,
                                g.solutions.count()))

    # Add rating trends for users.
    for (u, password) in report['users']['submitted_solution']:
        s = random.randint(1, 5)
        for solution in u.solutions.all():
            random_user = random.choice(random.choice(Company.objects.all()).group.user_set.all())
            rating = SolutionRating.objects.create(
                solution=solution,
                rater=random_user,
                score=s
            )
            s += random.choice([-1, 0, 1, 1])
            log.debug('new rating: {}'.format(rating))

    # Add some comments to gleanings.
    for g in random_subset(report['gleanings']['all'], k_pct_min=0.5,
                           k_pct_max=0.5):
        thread = fake_comment_thread(gleaning=g)
        report['comment_threads']['gleanings'].append(thread)

    all_solutions = list(Solution.objects.all())
    # Add some comments to solutions.
    for s in random_subset(all_solutions, k_pct_min=0.5,
                           k_pct_max=0.5):
        thread = fake_comment_thread(solution=s)
        report['comment_threads']['solutions'].append(thread)

    # Close some of the gleanings out
    report['gleanings']['closed'] = random_subset(report['gleanings']['all'])
    for g in report['gleanings']['closed']:
        g.closed = fake.date_time_this_month()
        log.debug('close out {}')
        g.save()

    report['gleanings']['with_selected'] = random_subset(list(report['gleanings']['with_solutions']), k_min=1)
    # Let some of the solutions be chosen
    for g in report['gleanings']['with_selected']:
        if g.closed:
            user = g.company.group.user_set.first()
            chosen = random.choice(list(g.solutions.all()))
            log.debug('selected {} for {}'.format(chosen, g))
            g.select(user, chosen)
            g.save()
    return report
