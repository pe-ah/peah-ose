import os

from pprint import pformat

from django.conf import settings
from django.contrib.auth.models import (
    User,
)

from django.db.models import (
    Count,
    Q,
    Min,
    Avg,
    Min,
    Max,

    IntegerField as IF
)
from django.utils import (
    timezone,
)
from datetime import (
    date,
    timedelta,
    datetime,
)
from dateutil import relativedelta

import pandas as pd
import numpy as np

from peah.models import (
    Gleaning,
    Solution,
    SolutionRating,
    UserRatingMean,
    SkillCount,
    PositionCount,
)
from peah.plotting import (
    plot,
)

from peah.storage import (
    PlotStorage
)


import logging; log = logging.getLogger(__name__)

def _per(start_date, end_date, field, step=1):
    d = start_date
    while d <= end_date:
        d = d + relativedelta.relativedelta(**{
            field: step,
        })
        yield d


def per_day(start_date, end_date, step=1):
    return _per(start_date, end_date, 'days', step)


def per_week(start_date, end_date, step=1):
    dif = start_date.weekday()
    start_date = start_date - relativedelta.relativedelta(days=dif)
    return _per(start_date, end_date, 'days', step)


def per_month(start_date, end_date, step=1):
    return _per(start_date, end_date, 'months', step)


def get_comment_date_range(user):

    q = '''SELECT
        0 AS id,
        MIN(peah_comment.when) AS minwhen,
        MAX(peah_comment.when) AS maxwhen
        FROM peah_comment
        WHERE peah_comment.user_id={}
    '''.format(user.id)

    date_data = user.comments.raw(q)
    return (date_data[0].minwhen, date_data[0].maxwhen)


def get_comments_within(user, min_date, max_date):
    return user.comments.filter(when__range=(min_date, max_date))


def get_comments_for_day(user, dt):
    d1 = timezone.datetime(year=dt.year, month=dt.month, day=dt.day,
                           hour=0, minute=0, second=0)
    d2 = d1 + relativedelta.relativedelta(days=1)
    return get_comments_within(user, d1, d2)


def get_comments_for_week(user, dt):
    d1 = timezone.datetime(year=dt.year, month=dt.month, day=dt.day,
                           hour=0, minute=0, second=0)
    d2 = d1 + relativedelta.relativedelta(days=7)
    return get_comments_within(user, d1, d2)


def get_comments_for_month(user, dt):
    d1 = timezone.datetime(year=dt.year, month=dt.month, day=1,
                           hour=0, minute=0, second=0)
    d2 = d1 + relativedelta.relativedelta(months=1)
    return get_comments_within(user, d1, d2)


def get_user_gleanings(user):
    return Gleaning.objects.filter(
        id__in=user.solutions.values('id').all()
    )


def get_comment_stats1(user):
    """ Get comment statistics as a pandas DataFrame """

    q = '''SELECT
        0 AS id,
        MIN(peah_comment.when) AS minwhen,
        MAX(peah_comment.when) AS maxwhen
        FROM peah_comment
        WHERE peah_comment.user_id={}
    '''.format(user.id)

    date_data = user.comments.raw(q)
    (min_d, max_d) = (date_data[0].minwhen, date_data[0].maxwhen)
    y = min_d.year
    m = min_d.month
    s = pd.DataFrame()
    x = 0
    log.debug('from {} to {}'.format(min_d, max_d))
    log.debug('{} <= {} and {} <= {}'.format(y, max_d.year, m, max_d.month))
    while y <= max_d.year and m <= max_d.month:
        start = datetime(year=y, month=m, day=1, tzinfo=timezone.get_current_timezone())
        when = start
        end = start + relativedelta.relativedelta(months=1)
        c = user.comments.filter(when__range=(start, end)).count()
        s.append({'x': x, 'when': when, 'count': c, }, ignore_index=True)
        log.debug('data={}'.format(pformat(s)))
        (y, m) = (end.year, end.month)
    return s


def get_comment_stats2(user):
    data = []

    (mindate, maxdate) = get_comment_date_range(user)
    for d in per_day(mindate, maxdate):
        s = d.strftime("%x")
        c = get_comments_for_day(user, d).count()
        data.append({"when": s, 'count': c, })

    return pd.DataFrame(data)


def save_plot(axis, *path_parts):
    path_parts = (str(pp) for pp in path_parts)
    path = os.path.join(settings.MEDIA_ROOT, *path_parts)
    if not path.endswith('*.png'):
        path = '{}.png'.format(path)
    if not os.path.exists(os.path.dirname(path)):
        os.makedirs(os.path.dirname(path))
    fig = axis.get_figure()
    fig.savefig(path)
    return path


def letter_grade(percent):
    # Thanks to https://www.cs.uni.edu/~mccormic/lettergrade.html
    letters = {
        0.94: 'A',
        0.90: 'A-',
        0.87: 'B+',
        0.83: 'B',
        0.80: 'B-',
        0.77: 'C+',
        0.73: 'C',
        0.70: 'C-',
        0.67: 'D+',
        0.63: 'D',
        0.60: 'D-',
        0.0: 'F',
    }
    for (k, v) in letters.items():
        if percent >= k:
            return v
    return None


def percent_between(a, b, x):
    if (a == b):
        return 0.0
    y = ((x - a) / (b - a))
    if y < 0:
        return 0.0
    return y


def get_scoring(user):
    # Get a list of all ratings
    k = 'mean_rating'
    means = UserRatingMean.objects.aggregate(min=Min(k),
                                             max=Max(k))
    user_mean = UserRatingMean.objects.filter(data__user=user)
    if not user_mean.count():
        log.debug('no count')
        return
    user_mean = user_mean.first()
    a = means['min']
    b = means['max']
    pct = percent_between(a, b, user_mean.mean_rating)
    letter = letter_grade(pct)
    letter_class = letter.replace('-', '_minus').replace('+', '_plus').lower()
    return {
        'percent': pct,
        'letter': letter,
        'class': letter_class,
    }


def get_user_skills(user):
    sk = SkillCount.objects.filter(data__user=user)\
                           .order_by('-value')
    ssk = sk[:5].as_series()
    log.debug('skill as series; {}'.format(ssk))
    storage = PlotStorage(user, SkillCount)
    if not ssk.empty:
        pie = ssk.plot.pie(autopct='%1.1f%%')
        p = storage.save_plot(pie)
        log.debug("Save plot to {}".format(p))
    return (sk, storage)


def get_user_stats(user):

    # Make sure data exists for the user. If no data exists, run the
    # analytics function.
    from peah.tasks import should_run, analyze_user_statistics
    if not (user.options.collect_statistics):
        return {}
    analyze_user_statistics([user.username, ])

    solutions = user.solutions

    dates = [s.submitted for s in solutions.all()]
    ticks = [solutions.filter(submitted__isnull=False,
                              submitted=d).count() for d in dates]
    scores = []
    for d in dates:
        solution = user.solutions.filter(submitted=d).first()
        if solution:
            scores.append(solution
                          .ratings
                          .aggregate(avgsc=Avg('score'))['avgsc'])

    log.debug('dates={}'.format(dates))
    log.debug('ticks={}'.format(ticks))

    di = pd.DatetimeIndex(dates)
    ds = pd.Series(ticks, index=di)
    rs = pd.Series(scores, index=di)
    #
    # if not ds.empty:
    #     save_plot(ds.plot(), 'plot', 'solutions', 'submitted', user.id)
    # if not rs.empty:
    #     save_plot(rs.plot(), 'plot', 'solutions', 'ratings', user.id)

    ratings = SolutionRating.objects.filter(
        solution__user=user
    )

    urm_storage = None
    if not rs.empty:
        try:
            urm_storage = PlotStorage(user, UserRatingMean)
            urm_storage.save_plot(rs.plot())
        except TypeError as te:
            log.warn('saving stats for {}: {}'.format(user, te))

    (sk, sk_storage) = get_user_skills(user)

    stats = {
        'anything': (get_scoring(user) is not None),
        'solutions': {
            'submitted': ds,
            'user': ratings.aggregate(average=Avg('score',
                                                  output_field=IF()),
                                      max=Max('score'),
                                      min=Min('score')),
            'comparison': get_scoring(user),
        },
        'plots': {
            'rating': urm_storage,
            'skills': sk_storage,
        }
    }
    return stats


def plot_time_model(qs, time_field, value_field):
    Model = type(qs.first())
    plot_func_name = getattr(Model, 'PLOT_FUNCTION', None)
    start = getattr(qs.first().time_field)
    end = getattr(qs.last().time_field)
    rng = pd.date_range(start=start, end=end)
