""" Utility methods for peah.

Contains helpful functions that don't belong anywhere else.
"""

import random
import yaml
import urllib3

from django.conf import settings
from django.shortcuts import (
    render,
    redirect,
)
from django import template
try:
    from django.urls import (path, include)
except ImportError as ie:
    from django.conf.urls import (url as path, include)
import os

import logging; log = logging.getLogger(__name__)

HERE = os.path.dirname(__file__)
NAME = 'peah'


def random_subset(population, k_min=0, k_max=None,
                  k_pct_min=None, k_pct_max=None):
    try:
        population = list(population)
    except Exception as e:
        log.error(e)
        return []
    if not isinstance(population, (set, list, tuple)) or len(population) == 0:
        return []
    if any([k_pct_min, k_pct_max]):
        (k_pct_min, k_pct_max) = sorted(
            [k_pct_min or 0.0, k_pct_max or 1.0]
        )
        k_pct_max = min(k_pct_max, 1.0)
        k_pct_min = max(k_pct_min, 0.0)
        k_pct = (k_pct_max - k_pct_min) * random.random() + k_pct_min
        # print('k_pct = {}'.format(k_pct))
        k = int(len(population) * k_pct)
    else:
        k_max = max(min(len(population), k_max or len(population)), 1)
        k_min = min(k_min, k_max)
        k = random.randint(k_min, k_max)
    return random.sample(population, k)


def ensure_app_tpl_dir_exists():
    directories = settings.TEMPLATES['DIRS']
    selected = os.path.join(os.path.abspath(directories[0]), NAME)
    for d in directories:
        if d.contains(NAME):
            selected = d
    if not os.path.exists(selected):
        os.makedirs(selected)
    return selected


def ensure_class_tpl_dir_exists(Class, app_tpl_dir):
    cls_dir = os.path.join(app_tpl_dir, Class.__name__)
    if not os.path.exists(cls_dir):
        os.makedirs(cls_dir)
    return cls_dir


def form_data_to_model_fields(ModelClass, data):
    """
    " Convert POST data to a model field dict.
    " Thanks to http://stackoverflow.com/questions/3106295/ddg#3106314
    """
    if data is None:
        return {}
    fields = [f.name for f in ModelClass._meta.get_fields()]
    return dict(tuple((k, v[0])
                for (k, v) in dict(data).items()
                if k in fields))


def update_model(instance, data, force=False, autosave=True):
    """ Update a model based on a dict of data.

    :param instance: model instance

    :param force: Force a field to be written even if it doesn't exist.
        WILL RAISE AN EXCEPTION!

    :param autosave: Save the model at the end.
    """
    log.info('-- before update: {}'.format(instance))
    for (k, v) in data.items():
        if force or hasattr(instance, k):
            log.debug('set {}: {} -> {}'.format(k, getattr(instance, k), v))
            setattr(instance, k, v)
    if autosave:
        instance.save()
    log.debug('-- after update: {}'.format(instance))


class ViewGenerator():

    def __init__(self, Class, template_base):
        self.Class = Class

    def tpl_path(self, action):
        return os.path.join(self.Class._meta.model_name, action + '.html')

    def render(self, request, action, *args, **kwargs):
        return render(request, self.tpl_path(action), *args, **kwargs)

    def generate_list(self):

        def f(request):
            # TODO: implement search
            instances = self.Class.query.all()
            return self.render(request, 'list', {
                self.Class._meta.verbose_name_plural: instances,
            })

        f.__name__ = 'create_{}'.format(self.Class.__name__.lower())

        return path(r'^/{}/list/$', f)

    def generate_create(self):

        def f(request):
            return self.render(request, 'create')

        f.__name__ = 'create_{}'.format(self.Class.__name__.lower())

        return f

    def generate_update(self):

        def f(request, id):
            instance = self.Class.objects.filter(id=id).first()
            if not instance:
                return redirect
            return render(request, )


def generate_crud_views(Class, create=True, read=True, update=True,
                        delete=True,
                        create_callback=None,
                        read_callback=None,
                        update_callback=None,
                        delete_callback=None):
    selected = ensure_app_tpl_dir_exists()
    cls_dir = ensure_class_tpl_dir_exists(Class, selected)
    mapping = {
        'create': create,
        'read': read,
        'update': update,
        'delete': delete,
    }

    views = []

    for (k, v) in mapping.items():
        if not v:
            continue
        crud_tpl = os.path.join(cls_dir, k + '.html')
        rel_path = os.path.join(NAME, Class.__name__, k + '.html')
        if os.path.exists(crud_tpl):
            continue
        with open(crud_tpl, 'w+') as tpl_file:
            tpl_file.write('{} {}'.format(k, Class.__name__))

        if (k == 'create') and not create_callback:

            def create_INSTANCE(request):
                return render(rel_path, request)

        if (k == 'read') and not read_callback:
            def read_INSTANCE(request, id=None):
                instance = Class.query.get(id=id)
                iname = Class.__name__.lower()
                return render(rel_path, {iname: instance, })

        if (k == 'update') and not update_callback:
            def update_INSTANCE(request, id=None):
                instance = None


class DepthDumper(yaml.Dumper):

    END_NODE = yaml.ScalarEvent(tag='tag:yaml.org,2002:null', value=u'??',
                                anchor=None, implicit=(True, True))

    def __init__(self, *args, **kwargs):
        self.max_depth = kwargs.pop('depth', None)
        super(DepthDumper, self).__init__(*args, **kwargs)
        self._curr_depth = 0

    @property
    def is_end(self):
        lvl = 0
        if self.indent:
            lvl = int(self.indent / 2) - 2
        return self.max_depth and lvl >= self.max_depth

    def emit(self, event):
        print('{}: {} - {}'.format(self.indent, event, self.is_end))
        if 'Start' in type(event).__name__:
            self._curr_depth += 1
        if 'End' in type(event).__name__:
            self._curr_depth -= 1
        if self.is_end:
            e = DepthDumper.END_NODE
            if hasattr(event, 'anchor'):
                e.anchor = event.anchor
            event = e
        super(DepthDumper, self).emit(event)



import io
def depth_dump_all(documents, depth=None, stream=None, Dumper=DepthDumper,
                   default_style=None, default_flow_style=None,
                   canonical=None, indent=None, width=None,
                   allow_unicode=None, line_break=None,
                   encoding=None, explicit_start=None, explicit_end=None,
                   version=None, tags=None):
        """
        Serialize a sequence of Python objects into a YAML stream.
        If stream is None, return the produced string instead.
        """
        getvalue = None
        if stream is None:
            if encoding is None:
                stream = io.StringIO()
            else:
                stream = io.BytesIO()
            getvalue = stream.getvalue
        dumper = Dumper(stream, default_style=default_style,
                        default_flow_style=default_flow_style,
                        canonical=canonical, indent=indent, width=width,
                        allow_unicode=allow_unicode, line_break=line_break,
                        encoding=encoding, version=version, tags=tags,
                        explicit_start=explicit_start,
                        explicit_end=explicit_end,
                        depth=depth)
        try:
            dumper.open()
            for data in documents:
                dumper.represent(data)
            dumper.close()
        finally:
            dumper.dispose()
        if getvalue:
            return getvalue()


def depth_dump(data, depth, stream=None, Dumper=DepthDumper, **kwds):
    """
    Serialize a Python object into a YAML stream.
    If stream is None, return the produced string instead.
    """
    return depth_dump_all([data], depth=depth, stream=stream, Dumper=Dumper, **kwds)


def listget(lst, i, default=None):
    if len(lst) >= i:
        return default
    return lst[i]


def auth_header(username, password):
    auth = "{}:{}".format(username, password)
    h = urllib3.util.make_headers(basic_auth=auth)
    return h['authorization']


def auth_header2(url_scheme):
    return auth_header(url_scheme.username, url_scheme.password)


from django.test.runner import DiscoverRunner


class NoDbTestRunner(DiscoverRunner):
    """ A test runner to test without database creation/deletion """

    def setup_databases(self, **kwargs):
        pass

    def teardown_databases(self, old_config, **kwargs):
        pass
