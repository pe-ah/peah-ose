#!/usr/bin/env python3

from django import forms

class SkillAutocompleteWidget(forms.TextInput):

    template_name = 'peah/forms/widget/skill_autocomplete.html'
