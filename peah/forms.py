#!/usr/bin/env python3
""" Pe'Ah Forms """

from django.conf import settings
# from django.contrib.auth.hashers import check_password

from crispy_forms.helper import FormHelper
from crispy_forms.layout import Submit

from ajax_select.fields import (
    AutoCompleteSelectMultipleField,
)

from django.contrib.auth.models import User
from django import (
    forms,
)
from haystack import forms as hsf
from haystack.query import SearchQuerySet

from peah.models import (
    Company,
    RepositoryBasedGleaning,
    UploadBasedGleaning,
    RepositoryBasedSolution,
    UploadBasedSolution,
    GleaningComment,
    SolutionComment,
    Gleaning,
)

from peah.util import listget

from django.contrib.auth import authenticate

import logging; log = logging.getLogger(__name__)


class CompanyForm(forms.ModelForm):

    name = forms.CharField()

    def __init__(self, data={}, *args, **kwargs):
        if kwargs.get('instance', None) is not None:
            kwargs['initial'] = kwargs.get('initial')
            data = dict(data or {})
            data['name'] = kwargs['instance'].name
            data['currency'] = kwargs['instance'].currency
        else:
            data = dict(data or {})
            data['currency'] = settings.DEFAULT_CURRENCY
        super(CompanyForm, self).__init__(data, *args, **kwargs)
        self.helper = FormHelper()
        self.helper.form_class = 'peah-forms'
        self.helper.form_method = 'post'
        create_button = Submit('create',
                               'Create Company',
                               css_class='btn btn-primary')
        save_button = Submit('save',
                             'Save Company',
                             css_class='btn btn-primary')
        if kwargs.get('initial', None) is not None:
            self.helper.add_input(save_button)
        else:
            self.helper.add_input(create_button)

    class Meta:
        model = Company
        fields = ['name', 'logo', 'currency', ]


class GleaningsForm(object):

    skills_required = AutoCompleteSelectMultipleField('skills')
    suitable_for = AutoCompleteSelectMultipleField('positions')
    closed = Submit('close', 'Close')

    def add_submit_buttons(self, *args, **kwargs):
        log.debug('args={}, kwargs={}'.format(args, kwargs))
        self.helper = FormHelper()
        self.helper.form_class = 'peah-forms'
        self.helper.form_method = 'POST'
        create_button = Submit('create',
                               'Create',
                               css_class='btn btn-primary')
        save_button = Submit('save',
                             'Save',
                             css_class='btn btn-primary')
        if kwargs.get('initial', kwargs.get('instance', None)) is not None:
            self.helper.add_input(save_button)
        else:
            self.helper.add_input(create_button)

    class Meta:
        fields = [
            'headline',
            'instructions',
            'skills_required',
            'suitable_for',
            'payout',
            'payout_hidden',
            'deadline',
            # 'closed',
        ]

        widgets = {
            'instructions': forms.Textarea(),
            # 'skills_required': autocomplete.ModelSelect2Multiple(url='skill_autocomplete'),
            'deadline': forms.SelectDateWidget(),
        }


class RepositoryBasedGleaningForm(forms.ModelForm, GleaningsForm):

    skills_required = GleaningsForm.skills_required
    suitable_for = GleaningsForm.suitable_for

    def __init__(self, *args, **kwargs):
        super(RepositoryBasedGleaningForm, self).__init__(*args, **kwargs)
        self.add_submit_buttons(*args, **kwargs)

    def save(self, commit=False):
        instance = super(RepositoryBasedGleaningForm, self).save(commit=False)
        if not getattr(instance, 'id', None):
            # needs to have a value for field "id" before this many-to-many
            # relationship can be used.
            return instance
        instance.skills_required.clear()
        instance.skills_required.add(*self.cleaned_data['skills_required'])
        instance.suitable_for.clear()
        instance.suitable_for.add(*self.cleaned_data['suitable_for'])
        return instance

    class Meta(GleaningsForm.Meta):
        model = RepositoryBasedGleaning
        fields = GleaningsForm.Meta.fields + [
            'repository',
        ]


class UploadBasedGleaningForm(forms.ModelForm, GleaningsForm):

    skills_required = GleaningsForm.skills_required
    suitable_for = GleaningsForm.suitable_for

    def __init__(self, *args, **kwargs):
        super(UploadBasedGleaningForm, self).__init__(*args, **kwargs)
        self.add_submit_buttons(*args, **kwargs)


    class Meta(GleaningsForm.Meta):
        model = UploadBasedGleaning
        fields = GleaningsForm.Meta.fields + [
            'maximum_upload_size',
        ]


class SolutionForm(object):

    def add_submit_buttons(self):
        self.helper = FormHelper()
        self.helper.form_class = 'peah-forms'
        self.helper.form_method = 'post'
        self.helper.add_input(
            Submit('save', 'Save', css_class='btn btn-secondary')
        )
        self.helper.add_input(
            Submit('submit', 'Submit', css_class='btn btn-primary')
        )

    class Meta:
        fields = [
            'details',
        ]


class RepositoryBasedSolutionForm(SolutionForm, forms.ModelForm):

    def __init__(self, *args, **kwargs):
        super(RepositoryBasedSolutionForm, self).__init__(*args, **kwargs)
        self.add_submit_buttons()

    class Meta(SolutionForm.Meta):
        model = RepositoryBasedSolution
        fields = SolutionForm.Meta.fields + [
            'repository',
        ]
        widgets = {
            'details': forms.Textarea,
        }


class UploadBasedSolutionForm(SolutionForm, forms.ModelForm):

    def __init__(self, *args, **kwargs):
        super(UploadBasedSolutionForm, self).__init__(*args, **kwargs)
        self.add_submit_buttons()

    class Meta(SolutionForm.Meta):
        model = UploadBasedSolution
        fields = SolutionForm.Meta.fields + [
            'file_upload',
        ]


class CommentForm(object):

    def add_submit_buttons(self):
        self.helper = FormHelper()
        self.helper.form_class = 'peah-forms peah-comment'
        self.helper.form_method = 'post'
        self.helper.add_input(
            Submit('send', 'Send', css_class='btn btn-primary')
        )

    class Meta:
        fields = [
            'text'
        ]


class SolutionCommentForm(CommentForm, forms.ModelForm):

    class Meta:
        model = SolutionComment
        fields = CommentForm.Meta.fields


class GleaningCommentForm(CommentForm, forms.ModelForm):

    class Meta:
        model = GleaningComment
        fields = CommentForm.Meta.fields


class BarePasswordForm(forms.Form):

    password = forms.CharField(widget=forms.PasswordInput)

    def add_submit_buttons(self):
        self.helper = FormHelper()
        self.helper.form_class = 'peah-forms'
        self.helper.form_method = 'post'
        self.helper.add_input(
            Submit('delete', 'Confirm Deletion', css_class='btn btn-error horiz-margin-medium')
        )

    def __init__(self, user, *args, **kwargs):
        super(BarePasswordForm, self).__init__(*args, **kwargs)
        self.user = user
        self.add_submit_buttons()

    def is_valid(self):
        is_valid = super(BarePasswordForm, self).is_valid()
        data = self.cleaned_data
        pw_ok = self.user.check_password(data['password'])
        log.debug('bare password OK ? {} and {}'.format(is_valid, pw_ok))
        return (is_valid and pw_ok)


class UserEditForm(forms.ModelForm):

    password = forms.CharField(widget=forms.PasswordInput(),
                               label="New Password",
                               required=False)
    password_confirm = forms.CharField(widget=forms.PasswordInput(),
                                       label="New Password (again)",
                                       required=False)
    current_password = forms.CharField(widget=forms.PasswordInput(),
                                       label="Current Password",
                                       help_text="To confirm changes.")

    def add_submit_buttons(self):
        self.helper = FormHelper()
        self.helper.form_class = 'peah-forms'
        self.helper.form_method = 'post'
        self.helper.add_input(
            Submit('save', 'Save',
                   css_class='btn btn-error horiz-margin-medium')
        )

    def __init__(self, *args, **kwargs):
        super(UserEditForm, self).__init__(*args, **kwargs)
        self.add_submit_buttons()

    def is_valid(self):
        is_valid = super(UserEditForm, self).is_valid()
        self.clean()
        if not is_valid:
            log.error(self.errors.as_json())
            return False
        data = self.cleaned_data
        pw_new_ok = (data['password'] is not None and
                     data['password'] == data['password_confirm'])
        pw_ok = authenticate(username=self.instance.username,
                             password=data['current_password'])
        if not pw_new_ok:
            self.add_error(('password', 'password_confirm'),
                           forms.ValidationError('Passwords do not match.'))
        if not pw_ok:
            self.add_error('current_password',
                           forms.ValidationError('Incorrect password.'))
        return (pw_new_ok and pw_ok)

    def save(self, *args, **kwargs):
        instance = getattr(self, 'instance', None)
        if not instance:
            raise AttributeError('`instance` needed')
        self.instance.email = self.cleaned_data['email']
        self.instance.username = self.cleaned_data['username']
        self.instance.set_password(self.cleaned_data['password'])
        self.instance.save()

    class Meta:
        model = User
        fields = ['email', 'username', 'password', 'password_confirm',
                  'current_password']


class GleaningsIndexSearchForm(hsf.SearchForm):

    company = forms.CharField(widget=forms.HiddenInput, required=False)
    position = forms.CharField(widget=forms.HiddenInput, required=False)
    skill = forms.CharField(widget=forms.HiddenInput, required=False)

    def add_submit_buttons(self):
        self.helper = FormHelper()
        # self.helper.form_class = 'peah-forms'
        self.helper.form_method = 'GET'
        self.helper.add_input(
            Submit('search', 'Search', css_class='btn btn-primary')
        )

    def modify_searchfield(self):
        classes = [
            'has-icon-right',
            # 'has-icon-left',
            # 'icon',
            'icon-search',
            'form-input',
        ]
        search_attrs = {
            'class': ' '.join(classes),
            'type': "search",
            'placeholder': "Search",
        }
        q = forms.CharField(widget=forms.TextInput(attrs=search_attrs),
                            label='',
                            required=False)
        self.fields['q'] = q

    def __init__(self, *args, **kwargs):
        super(GleaningsIndexSearchForm, self).__init__(*args, **kwargs)
        self.modify_searchfield()
        self.add_submit_buttons()

    def search(self):
        # First, store the SearchQuerySet received from other processing.
        sqs = super(GleaningsIndexSearchForm, self).search()
        # import pdb; pdb.set_trace()
        q = self.cleaned_data.get('q', None)
        company = self.cleaned_data.get('company', None)
        skill = self.cleaned_data.get('skill', None)
        position = self.cleaned_data.get('position', None)
        queryset = SearchQuerySet().models(RepositoryBasedGleaning,
                                           UploadBasedGleaning)
        # import pdb; pdb.set_trace()
        if q:
            queryset = queryset.filter(text=q)\
                               .filter_or(headline=q)\
                               .filter_or(company=q)\
                               .filter_or(skills_required=q)\
                               .filter_or(suitable_for=q)
        # import pdb; pdb.set_trace()
        if company and len(company):
            queryset = queryset.filter_and(company=company)
        if skill and len(skill):
            queryset = queryset.filter_and(skills_required=skill)
        if position and len(position):
            queryset = queryset.filter_and(suitable_for=position)
        return queryset


class GleaningSearchForm(hsf.ModelSearchForm):
    def get_models(self):
        """Return a list of the selected models."""
        return [UploadBasedGleaning, RepositoryBasedGleaning]
