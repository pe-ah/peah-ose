import random
import os

from django.core.mail import send_mail
from django.conf import settings

from django.test import (
    TestCase,
    RequestFactory,
    SimpleTestCase,
)
from unittest import (
    TestCase as PyTestCase
)
from django.contrib.auth.models import (
    User
)
from django.core.urlresolvers import reverse_lazy
from django.urls import reverse
from django.core.exceptions import (
    PermissionDenied,
)

from django.db.models import (
    Count,
)

from peah.models import (
    Company,
    Gleaning,
    RepositoryBasedGleaning as RGleaning,
    Solution,

    UploadBasedSolution as USolution,

    Comment,
    SolutionComment,
    GleaningComment,

    UserOptions,
    StatRequestQueue,

    MonthlyCommunicationData,
    WeeklyCommunicationData,
    DailyCommunicationData,
    WeeklySelectedSolutionData,
    DailySelectedSolutionData,

    SkillCount,
    PositionCount,

    get_user_companies,
    get_latest_user_solutions
)

from peah import views as PeahViews
from peah.util import (
    random_subset,
)
from peah.fake import (
    fake_money,

    fake_options,
    fake_user,
    fake_group,

    fake_company,

    fake_skill,
    fake_position,

    fake_upload_based_gleaning,
    fake_repository_based_gleaning,
    fake_repository_solution,
    fake_solution_collection,

    fake_comment,
    fake_comment_thread
)

from peah.tasks import (
    analyze_monthly_communication,
    analyze_weekly_communication,
    analyze_daily_communication,
    analyze_user_statistics,

    analyze_skills_percent,
)


from django.contrib.auth import authenticate

import logging; log = logging.getLogger(__name__)
from faker import Faker; fake = Faker()


class TestCustomUserModel(TestCase):

    def test_custom_user_model(self):
        regular_user = fake_user()

        company = fake_company()
        company_user = company.group.user_set.first()

        gleaning = fake_upload_based_gleaning(company=company)
        solution = USolution.objects.create(gleaning_id=gleaning.id,
                                            user_id=regular_user.id)

        self.assertEqual(get_user_companies(regular_user).count(), 0)
        self.assertEqual(get_latest_user_solutions(regular_user).first(),
                         solution)

        self.assertEqual(get_user_companies(company_user).count(), 1)
        self.assertEqual(get_latest_user_solutions(company_user).count(), 0)


class TestCompanyGleaning(TestCase):

    def test_company_creation(self):
        """ A company can be created. """
        company = fake_company()
        self.assertEqual(Company.objects.count(), 1)

    def test_company_gleaning(self):
        """ A position can be created at a company. """
        gleaning = fake_repository_based_gleaning()
        self.assertEqual(RGleaning.objects.count(), 1)
        self.assertGreater(gleaning.skills_required.count(), 0)
        self.assertGreater(gleaning.suitable_for.count(), 0)

    def test_solutions(self):
        """ Even with several solutions, the gleaning can list them. """
        initial_solution = fake_repository_solution()
        gleaning = initial_solution.gleaning
        self.assertEqual(type(gleaning), RGleaning)
        fake_repository_solution(gleaning=gleaning)
        fake_repository_solution(gleaning=gleaning)
        self.assertEqual(gleaning.solutions.count(), 3)

    def test_solution_and_selection(self):
        """ A company can select a solution as a chosen gleaning. """
        gleaning = fake_repository_based_gleaning()
        solutions = [
            fake_repository_solution(gleaning=gleaning)
            for i in range(0, random.randint(1, 10))
        ]
        solution = random.choice(solutions)
        gleaning = solution.gleaning
        company = gleaning.company
        user = company.group.user_set.first()
        log.info("{} chose {}'s solution".format(
            company, solution.user.username
        ))
        gleaning.select(user, solution)
        self.assertEqual(gleaning.selected, solution)
        self.assertTrue(solution.is_selected)

    def test_get_all_gleanings(self):
        """ Get both types of gleanings. """
        cbs = [fake_repository_based_gleaning, fake_upload_based_gleaning]
        generated_gleanings = []
        for i in range(0, random.randint(1, 10)):
            generated_gleanings.append(random.choice(cbs)())
        self.assertEqual(Gleaning.objects.count(), len(generated_gleanings))


class TestComments(TestCase):

    def test_comment_thread_creation(self):
        """A comment thread can be created."""
        gleaning = fake_repository_based_gleaning()
        thread = fake_comment_thread(gleaning=gleaning)
        all_comments = list(Comment.objects.all())
        self.assertEqual(len(thread), len(all_comments))

    def test_solution_comment_thread_failure(self):
        """ User not part of solution cannot comment. """
        solution = fake_repository_solution()
        user = fake_user()
        with self.assertRaises(PermissionDenied):
            SolutionComment.objects.create(user=user,
                                           text=fake.sentence(),
                                           solution=solution)


class TestViews(TestCase):

    def setUp(self):
        self.solutions = fake_solution_collection()
        self.factory = RequestFactory()

    def test_list_gleanings(self):
        request = self.factory.get('/')
        request.current_page = None  # Fake the middleware.
        response = PeahViews.list_gleanings(request)
        gleanings_count = Gleaning.objects.count()
        h1 = "{} Gleanings".format(gleanings_count)
        self.assertContains(response, h1)

    def test_edit_company_GET_success(self):
        """ A user that owns a company can view the Edit Company page """
        company = fake_company()
        new_name = fake.company()
        company_user = company.group.user_set.first()
        request = self.factory.get(reverse(PeahViews.edit_company,
                                           args=[company.id]))
        request.user = company_user
        request.current_page = None
        response = PeahViews.edit_company(request, company.id)
        self.assertEqual(response.status_code, 200)

    def test_edit_company_GET_failure(self):
        """ A user that owns a company CANNOT view the Edit Company page """
        company = fake_company()
        new_name = fake.company()
        company_user = company.group.user_set.first()
        request = self.factory.get(reverse(PeahViews.edit_company,
                                           args=[company.id]))
        request.user = fake_user()
        request.current_page = None
        with self.assertRaises(PermissionDenied):
            response = PeahViews.edit_company(request, company.id)

    def test_edit_company_POST_success(self):
        """ A user that owns a company can edit it."""
        company = fake_company()
        new_name = fake.company()
        company_user = company.group.user_set.first()
        request = self.factory.post(reverse(PeahViews.edit_company,
                                            args=[company.id]), {
                                            'name': new_name,
                                            })
        request.user = company_user
        request.current_page = None
        response = PeahViews.edit_company(request, company.id)

        # Assert company name has changed.
        company = Company.objects.filter(id=company.id).first()
        self.assertEqual(company.name, new_name)

        self.assertEqual(response.status_code, 302)  # redirect

    def test_edit_company_POST_failure(self):
        """ Company cannot be modified by a non-company user.

        We've basically already tested this with the original GET request, but
        just for thoroughness we'll test again.
        """
        company = fake_company()
        company_name = company.name
        company_id = company.id
        new_name = fake.company()
        # company_user = company.group.user_set.first()
        request = self.factory.post(reverse(PeahViews.edit_company,
                                            args=[company.id]), {
                                            'name': new_name,
                                            })
        request.user = fake_user()
        request.current_page = None
        with self.assertRaises(PermissionDenied):
            PeahViews.edit_company(request, company.id)

        company = Company.objects.filter(id=company_id).first()
        self.assertEqual(company.name, company_name)

    def test_create_repository_based_gleaning_GET_success(self):
        v = PeahViews.create_gleaning
        """ User can retrieve a form to create a repository-based gleaning. """
        company = fake_company(status=Company.Status.APPROVE)
        url = reverse(v)
        request = self.factory.get(url, {
            'type': 'repository',
        })
        request.current_page = None
        request.user = company.group.user_set.first()
        response = v(request)
        self.assertEqual(response.status_code, 200)

    def test_create_repository_based_gleaning_POST_success(self):
        """ User can retrieve a form to create a repository-based gleaning. """
        v = PeahViews.create_gleaning
        company = fake_company(status=Company.Status.APPROVE)
        skills = [fake_skill() for i in range(1, random.randint(2, 10))]
        skill_set = random_subset(skills, k_min=1)
        positions = [fake_position() for i in range(1, random.randint(2, 10))]
        positions_set = random_subset(positions, k_min=1)
        url = reverse(v)
        headline = fake.sentence()  # retrieve it after creation.
        post_data = {
            'headline': headline,
            'instructions': fake.paragraph(),
            'skills_required': [s.text for s in skill_set],
            'suitable_for': [p.text for p in positions_set],
            'payout': fake_money(),
            'payout_hidden': fake.boolean(),
            'maximum_upload_size': random.randint(0, 1024),
            'repository': fake.url(),
            'type': 'repository',
        }
        log.debug('post data: {}'.format(post_data))
        request = self.factory.post(url, post_data)
        request.user = company.group.user_set.first()
        request.current_page = None
        response = v(request)
        self.assertEqual(response.status_code, 302)
        gleaning = Gleaning.objects.filter(headline=headline).first()
        self.assertIsNotNone(gleaning)

    def test_new_solution_GET_success(self):
        v = PeahViews.new_solution
        gleaning = fake_repository_based_gleaning()
        url = reverse(v, args=[gleaning.id])
        request = self.factory.get(url)
        request.user = fake_user()
        request.current_page = None
        response = v(request, gleaning.id)
        self.assertEqual(response.status_code, 200)

    def test_new_solution_SAVE_success(self):
        v = PeahViews.new_solution
        gleaning = fake_repository_based_gleaning()
        url = reverse(v, args=[gleaning.id])
        post_data = {
            'save': 1,
            'details': fake.sentence(),
            'repository': fake.url(),
        }
        request = self.factory.post(url, post_data)
        request.user = fake_user()
        request.current_page = None
        response = v(request, gleaning.id)
        self.assertEqual(response.status_code, 302)
        # html = response.content.decode('ascii')
        # log.debug('response html: {}'.format(html))
        # self.assertIn(post_data['details'], html)
        solution = gleaning.solutions.first()
        self.assertIsNone(solution.submitted)

    def test_edit_solution_GET_success(self):
        v = PeahViews.edit_solution
        solution = fake_repository_solution()
        log.debug('solution: {}'.format(solution.id))
        url = reverse(v, args=[solution.id])
        request = self.factory.get(url)
        request.user = solution.user
        request.current_page = None
        response = v(request, solution.id)
        self.assertEqual(response.status_code, 200)
        # self.assertIn(solution.details, response.content.decode('utf-8'))

    def test_edit_solution_POST_user_failure(self):
        """ A user cannot edit a solution he did not create. """
        v = PeahViews.edit_solution
        solution = fake_repository_solution(is_submitted=False)
        url = reverse(v, args=[solution.id])
        post_data = {
            'save': 1,
            'details': fake.sentence(),
        }
        request = self.factory.post(url, post_data)
        request.user = fake_user()
        request.current_page = None
        with self.assertRaises(PermissionDenied):
            v(request, solution.id)

    def test_edit_solution_POST_submitted_failure(self):
        """ A user cannot edit a solution that's already been submitted. """
        v = PeahViews.edit_solution
        solution = fake_repository_solution(is_submitted=True)
        url = reverse(v, args=[solution.id])
        post_data = {
            'save': 1,
            'details': fake.sentence(),
        }
        request = self.factory.post(url, post_data)
        request.user = solution.user
        request.current_page = None
        with self.assertRaises(PermissionDenied):
            v(request, solution.id)

    def test_edit_solution_SAVE_submitted_success(self):
        """ A user cannot edit a solution that's already been submitted. """
        v = PeahViews.edit_solution
        solution = fake_repository_solution(is_submitted=False)
        url = reverse(v, args=[solution.id])
        # original_details = solution.details
        new_details = fake.sentence()
        post_data = {
            'save': 1,
            'details': new_details,
        }
        request = self.factory.post(url, post_data)
        request.user = solution.user
        request.current_page = None
        response = v(request, solution.id)
        self.assertEqual(response.status_code, 200)

    def test_edit_solution_SUBMIT_submitted_success(self):
        """ User can successfully submit a solution. """
        v = PeahViews.confirm_solution
        solution = fake_repository_solution(is_submitted=False)
        url = reverse(v, args=[solution.id])
        # original_details = solution.details
        new_details = fake.sentence()
        post_data = {
            'yes': 1,
            'details': new_details,
        }
        request = self.factory.post(url, post_data)
        request.user = solution.user
        request.current_page = None
        response = v(request, solution.id)
        self.assertEqual(response.status_code, 302)
        solution = Solution.objects.filter(id=solution.id).first()
        self.assertIsNotNone(solution.submitted)

    def test_select_solution_GET_unsubmitted_failure(self):
        """ Unsubmitted solution failure test.

        company cannot select solution because it hasn't been submitted
        """
        solution = fake_repository_solution(is_submitted=False)
        v = PeahViews.select_solution
        url = reverse(v, args=[solution.id])
        request = self.factory.get(url)
        request.user = solution.gleaning.company.group.user_set.first()
        request.current_page = None
        with self.assertRaises(PermissionDenied):
            v(request, solution.id)

    def test_select_solution_GET_unowned_failure(self):
        """ Unowned failure test.

        Company cannot select solution because it's not owned by company.
        """
        solution = fake_repository_solution(is_submitted=True)
        wrong_company = fake_company()
        wrong_user = wrong_company.group.user_set.first()
        v = PeahViews.select_solution
        url = reverse(v, args=[solution.id])
        request = self.factory.get(url)
        request.user = wrong_user
        request.current_page = None
        with self.assertRaises(PermissionDenied):
            v(request, solution.id)

    def test_select_solution_GET_submitted_success(self):
        """ Company can get solution page for solution of own gleaning. """
        solution = fake_repository_solution(is_submitted=True)
        v = PeahViews.select_solution
        url = reverse(v, args=[solution.id])
        request = self.factory.get(url)
        request.user = solution.gleaning.company.group.user_set.first()
        request.current_page = None
        response = v(request, solution.id)
        self.assertEqual(response.status_code, 200)

    def test_select_solution_POST_submitted_confirm_success(self):
        """ Company can get solution page for solution of own gleaning. """
        solution = fake_repository_solution(is_submitted=True)
        v = PeahViews.select_solution
        url = reverse(v, args=[solution.id])
        post_data = {
            'yes': 1,
        }
        request = self.factory.post(url, post_data)
        request.user = solution.gleaning.company.group.user_set.first()
        request.current_page = None
        response = v(request, solution.id)
        self.assertEqual(response.status_code, 302)
        solution = Solution.objects.filter(id=solution.id).first()
        self.assertIsNotNone(solution.gleaning.selected)
        self.assertEqual(solution.gleaning.selected, solution)

    def test_account_edit_SUCCESS(self):
        orig_password = fake.password()
        user = fake_user(password=orig_password)
        v = PeahViews.edit_account_info
        url = reverse(v)
        new_password = fake.password()
        post_data = {
            'username': user.username,
            'email': user.email,
            'password': new_password,
            'password_confirm': new_password,
            'current_password': orig_password,
        }
        request = self.factory.post(url, post_data)
        request.user = user
        request.current_page = None
        response = v(request)
        user.refresh_from_db()
        self.assertEqual(response.status_code, 200)
        self.assertTrue(authenticate(username=user.username,
                                     password=new_password))

    def test_list_gleanings_search(self):
        v = PeahViews.ListGleanings.as_view()
        url = reverse_lazy('list_gleanings')
        gleaning = fake_upload_based_gleaning()
        term = random.choice(gleaning.headline.split(" "))
        get_data = {
            'q': term,
        }
        request = self.factory.get(url, get_data)
        request.current_page = None
        response = v(request)
        self.assertEqual(response.status_code, 200)


class TestAnalytics(TestCase):

    def setUp(self):
        self.ok_user = fake_user(include_options=False)
        self.ok_options = UserOptions.objects.create(
            user=self.ok_user,
            collect_statistics=True,
            show_statistics=UserOptions.Show.YES,
        )
        analyze_user_statistics()

    def test_skill_analytics(self):
        skills = [
            fake_skill() for i in range(0, 10)
        ]
        solutions = [
            fake_repository_solution(user=self.ok_user, skills=skills)
            for i in range(0, 30)
        ]
        analyze_skills_percent(self.ok_user.id, True)
        sp = SkillCount.objects.filter(data__user=self.ok_user)
        log.info("The result: {}".format(sp))
        self.assertIsNotNone(sp)

        series = sp.as_series()
        self.assertEqual(series.count(), len(skills))


    def test_communication_data(self):
        c = None
        for i in range(0, 10):
            solution = fake_repository_solution(user=self.ok_user)
            c = fake_comment(self.ok_user, after_comment=c,
                             solution=solution)
        dcd = DailyCommunicationData.objects.filter(data__user=self.ok_user)
        wcd = WeeklyCommunicationData.objects.filter(data__user=self.ok_user)
        mcd = MonthlyCommunicationData.objects.filter(data__user=self.ok_user)
        self.assertEqual(dcd.count(), wcd.count())
        self.assertEqual(wcd.count(), mcd.count())

    def test_queryset_implementation(self):
        solution = fake_repository_solution()
        comments = fake_comment_thread(solution=solution)
        analyze_user_statistics(force=True)
        user = User.objects\
                   .annotate(comments_count=Count('comments'))\
                   .filter(comments_count__gt=0).first()
        drs = DailyCommunicationData.objects.as_daterange_series()
        self.assertIsNotNone(drs)


class TestNonDatabase(TestCase):

    def test_email(self):
        self.test_address = os.getenv('DJANGO_TEST_EMAIL')
        self.assertIsNotNone(self.test_address)
        log.debug("Sending test email...")
        r = send_mail(subject='Django Unittest',
                      message='this is a test',
                      from_email=settings.DEFAULT_FROM_EMAIL,
                      recipient_list=[self.test_address])
        self.assertEqual(r, 1)
        log.info("EMAIL SENT! {}".format(r))
