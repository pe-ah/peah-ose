==================
User Documentation
==================
:Author: Jordan Hewitt <jordan.hewitt@peah.app>
:Date: 2018-11-27

Introduction
============

This introduction describes preliminary housekeeping information, such as
purpose and terminology.

Purpose
-------

Headine
*******

  Hire the right candidate by getting things done.

Mission Statement
*****************

  Pe'Ah takes the guesswork out of hiring by inviting job candidates to
  actively get behind your company's mission and values and display their
  skills concretely--all before you see their |resume|.

Why Pe'Ah?
**********

Within the last two decades hiring has changed drastically. Application Tracking
Systems have taken the place of looking over stacks of |resume|s.

While this strategy might be ideal for the majority of blue-collar jobs, it's
detrimental to specialized fields, such as high tech. All a terrible candidate
needs to do is game the ATS system by feeding false information, lie for the
interview, and score a job.

Looking Backward|helip|
#########################

Those who are devoutly Jewish or Biblical scholars might recognize the term
"Pe'Ah."

It was a practice done by Israelites. In order to promote a sense of community
and belonging, Israelite farmers were commissioned to purposfully
leave part of their fields unharvested. These portions are called "Gleanings."

This is a very bad business decision. So why did they do this?

For the poor.

The poor who aren't able to sustain themselves, who need some way to earn
a living.

|helip|To See the Future
#########################

Who are the poor among us today?

Those who are either looking for work or underemployed.

So then what would these gleanings be?

That's where companies have the opportunity to play farmer. What a gleaning is
may look different for each company. Is there an SQL query your team has been
working hard to resolve? Is there a widget your UX team hasn't found the right
CSS styling for?

A gleaning should be a non-critical project (or part of a non-critical project)
that can be given to job candidates.

Want to attract the right candidate even more? Offer a **payout**, which can
attract more job candidates to solve your gleaning.

Terminology
-----------

:company:
  Organization of people.

  .. sidebar: Developer Note
    An Organization is a 1-to-1 relationship with a Django `Group` model
    The group name is the name of the organzation.

:gleaning:
  The basis for Pe'Ah. Gleanings are parts or projects, or entire projects
  that a company wants candidates to work on. There are 2 types of gleanings:

  :repository-based gleanings:
    These are gleanings that are part of a repository
    (e.g. SVN, GitHub, BitBucket).

  :upload-based gleanings:
    These are gleanings that aren't under revision control but use a file or
    package as a starting point.

:solution:
  If the gleaning is the question, the solution is the answer. Solutions are
  a job candidate's potential resolution to the company's problem.

:payout:
  Amount paid to a job candidate when a solution is chosen. Note that only
  one (1) solution per gleaning may be chosen.

Pe'Ah System
============

The following section walks throught he various components of Pe'Ah and shows
how to navigate the system.

Front Page: Gleanings Search
----------------------------

.. image:: images/screenshots/front/1.png

The front page contains gleanings from all companies registered on
`peah.app <https://peah.app>`_ or your company's instance of **Pe'Ah**.

The top of the page is the *navbar*, which has common links and the user menu.

User Menu
*********

The user menu is on the right side of the navbar with the user icon
(|usericon|). The menu will be different depending on whether the user is logged
in and the specific user role.

.. |helip| replace:: …
.. |resume| replace:: résumé
