# see https://hub.docker.com/_/elasticsearch/
NETWORK=elasticnetwork
VERSION=6.4.3
docker run -d --name elasticsearch --net $NETWORK -p 9200:9200 -p 9300:9300 -e "discovery.type=single-node" docker.elastic.co/elasticsearch/elasticsearch:${VERSION}
