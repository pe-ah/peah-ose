import os
import re
from setuptools import (
    setup,
    find_packages,
)

HERE = os.path.join(os.path.dirname(__file__))
REQUIREMENTS = os.path.join(HERE, 'requirements.txt')

RE_REQ = re.compile(r'^[\w\d\-\_]+').search

# Utility function to read the README file.
# Used for the long_description.  It's nice, because now 1) we have a top level
# README file and 2) it's easier to type in the README file than to put a raw
# string in below ...
def read(fname):
    return open(fname).read()

def read_requirements():
    all_reqs = []
    with open(REQUIREMENTS) as reqfile:
        for line in reqfile:
            r = RE_REQ(line)
            if not r:
                print('skipping {}'.format(line))
                continue
            all_reqs.append(r[0])
        return all_reqs


setup(
    name="peah",
    version="0.0.3",
    author="Jordan Hewitt",
    author_email="jordan.hewitt@peah.app",
    description=("Hire qualified candidates by getting things done."),
    keywords="hiring career app",
    url="http://peah.app",
    packages=find_packages(),
    include_package_data=True,
    # install_requires=read_requirements(),
    long_description=read(os.path.join(HERE, 'README')),
    classifiers=[
        "Development Status :: 3 - Alpha",
        "Topic :: Utilities",
        "License :: OSI Approved :: BSD License",
    ],
)
